const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
var path = require('path')

// importLoader:1 from https://blog.madewithenvy.com/webpack-2-postcss-cssnext-fdcd2fd7d0bd

module.exports = {
    // devtool: 'source-map', // No need for dev tool in production

  module: {
    rules: [{
      test: /\.json?$/,
      loader: 'json'
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        exclude: [
          path.resolve(__dirname, '/django/src/static/resources/')
        ],
        use: [
          {
            loader: 'css-loader',
            query: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[local]__[hash:base64:5]'
            }
          },
          'postcss-loader'
        ]
      })
    }, {
      test: /\.scss$/, // sass support
      exclude: [
        path.resolve(__dirname, '/django/src/static/resources/')
      ],
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: true,
            localIdentName: '[local]___[hash:base64:5]'
          }
        },
        'sass-loader'
      ]
    },
    { test: /\.jpg$/, loader: 'file-loader' },
    { test: /\.png$/, loader: 'url-loader?mimetype=image/png' },
    { test: /\.svg$/,
      loader: 'svg-url-loader' // the loader used for svg support
    }, {
      test: /\.css$/,
      include: [
        path.resolve(__dirname, '/django/src/static/resources/styles')
      ],
      use: ExtractTextPlugin.extract([
        {
          loader: 'css-loader',
          options: { importLoaders: 1 }
        },
        'postcss-loader']
            )
    }, {
      test: /\.scss$/,
      include: [
        path.resolve(__dirname, '/django/src/static/resources/styles')
      ],
      use: ExtractTextPlugin.extract([
        {
          loader: 'css-loader',
          options: { importLoaders: 1 }
        },
        'postcss-loader',
        {
          loader: 'sass-loader',
          options: {
            data: `@import "${__dirname}/../src/static/resources/styles/config/_variables.scss";`
          }
        }]
            )
    }]
  },

  plugins: [
    new ExtractTextPlugin('styles/[name].css'),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
}
