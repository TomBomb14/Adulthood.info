const ExtractTextPlugin = require('extract-text-webpack-plugin')
var webpack = require('webpack')
var path = require('path')
// importLoader:1 from https://blog.madewithenvy.com/webpack-2-postcss-cssnext-fdcd2fd7d0bd
console.log('path', path.resolve(__dirname, '/django/src/static/components/'))
module.exports = {
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.svg$/,
        loader: 'url-loader' // the loader used for svg support
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }, { test: /\.css$/,
        exclude: [
          path.resolve(__dirname, '/django/src/static/resources/')
        ],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[local]___[hash:base64:5]'
            }
          }
        ]
      }, {
        test: /\.scss$/, // sass support
        exclude: [
          path.resolve(__dirname, '/django/src/static/resources/')
        ],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[local]___[hash:base64:5]'
            }
          },
          'sass-loader'
        ]
      }, {
        test: /\.css$/,
        include: [
          path.resolve(__dirname, '/django/src/static/resources/styles')
        ],
        use: ExtractTextPlugin.extract([
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader']
            )
      }, {
        test: /\.scss$/,
        include: [
          path.resolve(__dirname, '/django/src/static/resources/styles')
        ],
        use: ExtractTextPlugin.extract([
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              data: `@import "${__dirname}/../src/static/resources/styles/config/_variables.scss";`
            }
          }]
            )
      }]
  },
  plugins: [
    new ExtractTextPlugin('styles/[name].css'),
    new webpack.HotModuleReplacementPlugin()
  ]
}
