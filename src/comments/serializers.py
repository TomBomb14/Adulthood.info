from rest_framework import serializers
#imports users and user serailzers 
from accounts.models import User
from accounts.serializers import UserWithIdSerializer
from posts.models import Post
from comments.models import Comment
from likes.models import CommentLikes
from django.db import models
from collections import OrderedDict
import datetime


#Seriaizer that accepts incoming user data and creates the Comment
class CommentCreationSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Comment
        fields = ('id', 'post_id','author', 'content', 'reply_id')
        
    #creates a comment with validated_data
    def create(self, validated_data):
            
        comment = Comment.objects.create(content = validated_data['content'], author = validated_data['author'],
                                        post_id = validated_data['post_id'], category=validated_data['post_id'].category)
        return comment
   
#Seriaizer that deletes a Comment
class CommentDeleteSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Comment
        fields = '__all__'

#Seriaizer that update a Commet
class CommentUpdateSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Comment
        fields = 'id', 'content'

           
#serailzer for  Comments that serailzer and deserailzer data
class CommentSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Comment
        fields = '__all__'

    def to_representation(self, instance):
        rep = {}
        
        # initialize on ordereddict
        rep=OrderedDict()
        rep['id'] = str(instance.id)
        rep['post_id'] = str(instance.post_id.id)
        rep['author'] = instance.author.__str__()
        rep['category'] = instance.category
        rep['content'] = instance.content
        rep['date_created'] = int(instance.date_created.timestamp())
        rep['reply_id'] = str(instance.reply_id)
        likes = CommentLikes.objects.filter(reply_id_id = instance.id)
        rep['number_of_likes']= len(likes)


        return rep


 
            





        

