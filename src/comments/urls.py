from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import comments.views


urlpatterns = [
    url(_(r'^CreateComment/$'),
        comments.views.CommentCreationView.as_view(),
        name='CreateComment'),
    url(_(r'^DeleteComment/(?P<id>[0-9]+)$'),
        comments.views.CommentDeleteView.as_view(),
        name='Delete'),
    url(_(r'^UpdateComment/(?P<id>[0-9]+)$'),
            comments.views.CommentUpdateView.as_view(),
            name='UpdateComment'),
    url(_(r'^comment/(?P<id>\d+)$'),
            comments.views.GetCommentView.as_view(),
            name='GetComment'),

]
