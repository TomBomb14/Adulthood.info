
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from accounts.models import User
from posts.models import Post

class Comment(models.Model):
    post_id = models.ForeignKey(Post, related_name = "post", on_delete=models.CASCADE)
    author = models.ForeignKey(User, related_name = "commentAuthor", on_delete=models.CASCADE)
    category = models.CharField(max_length = 50)
    content = models.TextField()
    date_created = models.DateTimeField(auto_now = True)
    reply_id = models.ForeignKey('self', blank = True, related_name = 'reply', null = True)
    
    def create_comment(self,post_id,author,category,content,date_created,reply_id):
        """
        Create and save comment.

        :param attached_post: Post
        :param author: User
        :param Category: string
        :param Content: textfield
        :param datecreated: date
        :param reply: Comment
        :param extra_fields:
        :return: comment
        """
        now = timezone.now()
        comment = self.model(post_id=attached_post,
                          author=author,
                          category=category,
                          content=content,
                          date_created=now,
                          reply_id=reply, 
                          )
        comment.save(using=self._db)
        return comment



    
      

