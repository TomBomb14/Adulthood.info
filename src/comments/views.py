from django.shortcuts import get_object_or_404
from django_rest_logger import log
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import GenericAPIView, UpdateAPIView , DestroyAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


from lib.utils import AtomicMixin
from django.http import QueryDict

from posts.models import Post
from posts.serializers import PostCreationSerializer, PostSerializer

from accounts.models import User
from accounts.serializers import UserSerializer, UserWithIdSerializer

from comments.models import Comment
from comments.serializers import CommentCreationSerializer, CommentSerializer, CommentDeleteSerializer, CommentUpdateSerializer

from rest_framework import authentication
from rest_framework import exceptions

class CommentCreationView(GenericAPIView):
    serializer_class = CommentCreationSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        """Comment Creation view."""    
        newComment = request.data.copy()
        newComment['author'] = request.user.id
        post = Post.objects.filter(pk=request.data["post_id"])


        serializer = CommentCreationSerializer(data=newComment)
        if serializer.is_valid():
            serializer.save()
            createdComment = Comment.objects.filter(id=serializer.data["id"])
            postComments = Comment.objects.filter(post_id = request.data["post_id"])

            allComments = createdComment | postComments
            cdata = CommentSerializer(allComments, many = True)
            pdata = PostSerializer(post, many = True)
            

            postAuthor = User.objects.filter(id__in = post.values('author'))
            commentsAuthor = User.objects.filter(id__in = postComments.values('author'))
            ActiveAuthor = User.objects.filter(id = request.user.id)

            allUsers = postAuthor | commentsAuthor | ActiveAuthor 

            uSerialzier = UserWithIdSerializer(allUsers, many = True)
           
            return Response({"users":uSerialzier.data,"posts":pdata.data,"comments":cdata.data},status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#view that is used to delete a new Comment
class CommentDeleteView(DestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentDeleteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        comment_id = str(instance.id)
        post_id = str(instance.post_id.id)
        self.perform_destroy(instance)
        return Response({"post_id": post_id, "id": comment_id})



#view that is used to update a  Comment
class CommentUpdateView(UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentUpdateSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'

    def put (self, request, id):
       comment = get_object_or_404(Comment, id =  id)
       comment.content = request.data['content']
       comment.save()
       serializer = CommentSerializer(comment)
       commentList = list()
       commentList.append(serializer.data)
       return Response({"comments":commentList})
       
        

class GetCommentView(GenericAPIView):
    serializer_class = CommentSerializer
    def get(self,request,id):
        comment = get_object_or_404(Comment, id=id)
        serailzer = CommentSerializer(comment)
        return Response({'comments':serailzer})


        
        

