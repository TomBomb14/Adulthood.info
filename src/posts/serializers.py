from rest_framework.serializers import ModelSerializer, ListSerializer
#imports users and user serailzers 
from accounts.models import User
from accounts.serializers import UserWithIdSerializer
from posts.models import Post
from django.db import models
from comments.models import Comment
from likes.models import PostLikes
from collections import OrderedDict
import datetime


#Seriaizer that accepts incoming user data and creates the user
class PostCreationSerializer(ModelSerializer):
  
    class Meta:
        model = Post
        fields = ('id', 'title','author', 'category', 'content', 'is_question')
        #creates a post with validated_data
        def create(self, validated_data):
          """
          Create the object.

          :param validated_data: string
          """
          return Post.objects.create(title=validated_data.title,
                                    author=validated_data.author,
                                    category=validated_data.category,
                                    content=validated_data.content,
                                    is_question=validated_data.is_question)
   
#Seriaizer that deletes a post
class PostDeleteSerializer(ModelSerializer):
  
    class Meta:
        model = Post
        fields = '__all__'

#Seriaizer that update a post
class PostUpdateSerializer(ModelSerializer):
  
    class Meta:
        model = Post
        fields = ('id', 'title', 'category' ,'content', 'is_question')
            
#custom list serailzer that is not in use was coded for changing json structure
class PostListSerailzer(ListSerializer):
    
    def to_representation(self, data):
    
        iterable = data.all() if isinstance(data, Post) else data
        _data = {}
        for item in iterable:
            _data[str(item.id)] = dict(self.child.to_representation(item))
        return _data

    


        
#serailzer for  posts that serailzer and deserailzer data
class PostSerializer(ModelSerializer):
    
    class Meta:
        #list_serializer_class = PostListSerailzer
        model = Post
        fields = ('id','title','author','category', 'content', 'is_question')
    
#customer representation that uses an orderdict to create a consistent structure for the json response
    def to_representation(self, instance):
        rep = {}


        # initialize on ordereddict
        rep=OrderedDict()
        rep['id'] = str(instance.id)
        rep['title'] = instance.title
        rep['author'] = instance.author.__str__()
        rep['category'] = instance.category
        rep['content'] = instance.content
        rep['date_created'] = int(instance.date_created.timestamp())
        rep['is_question'] = str(instance.is_question)
        likes = PostLikes.objects.filter(post_id_id = instance.id)
        rep['number_of_likes']= len(likes)


        comments = Comment.objects.filter(post_id = instance.id)
        postComments = list()

        #Append any comment ids to postComments
        for c in comments:
            postComments.append(str(c.id))
        rep['comments'] = postComments
        return rep
 
            





        

