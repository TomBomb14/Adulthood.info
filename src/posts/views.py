from django.shortcuts import get_object_or_404
from django.http import HttpResponseNotFound, Http404
from django_rest_logger import log
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import GenericAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.models import User
from accounts.serializers import UserSerializer, UserWithIdSerializer
from lib.utils import AtomicMixin
from django.http import QueryDict

from posts.models import Post
from posts.serializers import PostCreationSerializer, PostSerializer, PostUpdateSerializer, PostDeleteSerializer

from comments.models import Comment
from comments.serializers import CommentCreationSerializer, CommentSerializer

from likes.models import PostLikes
from likes.serializers import PostLikeSerializer

from rest_framework import authentication
from rest_framework import exceptions

from itertools import chain

#view that is used to create a new post
class PostCreationView(GenericAPIView):
    serializer_class = PostCreationSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """Post Creation view."""
        
        #user= User.objects.get(request.Post['author'])
        #post=Post.objects.latest('date_created')
        if request.user.is_active == False:
            return Response(status = status.HTTP_400_BAD_REQUEST)
        request.data['author'] = request.user.id

        serializer = PostCreationSerializer(data=request.data)
        uSerializer = UserWithIdSerializer(request.user)
        if serializer.is_valid():
            serializer.save()
            post = Post.objects.get(pk=serializer.data["id"])
            pdata = PostSerializer(post)
            UresponseList = list()
            UresponseList.append(uSerializer.data)
            PresponseList = list()
            PresponseList.append(pdata.data)
            return Response({"users":UresponseList,"posts":PresponseList},status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#view that is used to delete a new post
class PostDeleteView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDeleteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_200_OK)
    
#view that is used to Update a new post
class PostUpdateView(UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostUpdateSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'
    
    def put (self, request, id):
        #reterives and updates the post
        if(request.data['is_question'] == True):
            Updatepost = Post.objects.filter(id = id).update(title = request.data['title'],category = request.data['category'],content = request.data['content'], is_question = True)
        else:
            Updatepost = Post.objects.filter(id = id).update(title = request.data['title'],category = request.data['category'],content = request.data['content'], is_question = False)
        post = get_object_or_404(Post, id = id)
        #grabs all comments and author related to the post
        user = User.objects.get(email=post.author)
        comments = Comment.objects.filter(post_id=post.id)
        user = User.objects.filter(email=post.author)
        comments = Comment.objects.filter(post_id=post.id).select_related('author')
        CommentUser = User.objects.filter(id__in = comments.values('author'))
        AllUsers = user | CommentUser
        #serailzers the data to be sent to front end
        serializer = PostSerializer(post)
        uSerializer = UserWithIdSerializer(AllUsers, many = True)
        cSerializer = CommentSerializer(comments, many=True)
        PresponseList = list()
        PresponseList.append(serializer.data)
        return Response({
        "users":uSerializer.data,
        "posts":PresponseList,
        "comments":cSerializer.data
        })

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        #Make request.data['author'] into uuid
        request.data['author'] = request.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

#gets a post by pk 
class GetPostView(GenericAPIView):
    serializer_class = PostSerializer

    def get(self, request, pk):

       # try:
        post = get_object_or_404(Post, pk=pk)
        user = User.objects.get(email=post.author)
        comments = Comment.objects.filter(post_id=post.id)
        user = User.objects.filter(email=post.author)
        comments = Comment.objects.filter(post_id=post.id).select_related('author')
        CommentUser = User.objects.filter(id__in = comments.values('author'))
        AllUsers = user | CommentUser
        serializer = PostSerializer(post)
        uSerializer = UserWithIdSerializer(AllUsers, many = True)
        cSerializer = CommentSerializer(comments, many=True)
        PresponseList = list()
        PresponseList.append(serializer.data)
        return Response({
        "users":uSerializer.data,
        "posts":PresponseList,
        "comments":cSerializer.data
        },status = status.HTTP_200_OK)



#view that  gets all the posts
class GetAllPostsView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get(self, request):
          
        posts = Post.objects.filter(is_question=False).select_related("author").order_by("-date_created")
        users = User.objects.filter(id__in=posts.values('author'))

        serializer = PostSerializer(posts, many=True)
        uSerializer = UserWithIdSerializer(users, many=True)
        
        return Response({
            'posts':serializer.data,
            'users':uSerializer.data
            
            
        })

    def post(self, request):
          
        posts = Post.objects.filter(is_question=False).select_related("author").order_by("-date_created")
        users = User.objects.filter(id__in=posts.values('author'))

        serializer = PostSerializer(posts, many=True)
        uSerializer = UserWithIdSerializer(users, many=True)
        
        return Response({
            'posts':serializer.data,
            'users':uSerializer.data
            
            
        })

    

    
        
#view that get all questions 
class GetAllQuestionsView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get(self, request):

        posts = Post.objects.filter(is_question=True).order_by('-date_created')
        users = User.objects.filter(id__in=posts.values('author'))

        serializer = PostSerializer(posts, many=True)
        uSerializer = UserWithIdSerializer(users, many=True)
        
        return Response({
            'posts': serializer.data,
            'users':uSerializer.data
            
            
        })

    def post(self, request):

        posts = Post.objects.filter(is_question=True).order_by('-date_created')
        users = User.objects.filter(id__in=posts.values('author'))

        serializer = PostSerializer(posts, many=True)
        uSerializer = UserWithIdSerializer(users, many=True)
        
        return Response({
            'posts': serializer.data,
            'users':uSerializer.data
            
            
        })
        
        
#view that checks if all is the category and serailzer the data and returns a response
class GetAllPostsByCategoryView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


    def get(self, request, category):
        """
            Looks like there is no user to pass info
            Need to be able to get userId

        """
        #import pdb
        #pdb.set_trace()
       

        
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=False).select_related("author").order_by("-date_created")
            users = User.objects.filter(id__in=posts.values('author'))

            
            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        else:   
            posts = Post.objects.filter(is_question=False,category=category).order_by('-date_created')
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)

       

        return Response({
            'posts':serializer.data,
            'users':uSerializer.data    
                
        })


    def post(self, request, category):

        """
            Looks like there is no user to pass info
            Need to be able to get userId

        """
        


            #userId = User.objects.filter()
            # Find the userId of the email
            # Find the postId with userId on Likes object

       

        
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=False).select_related("author").order_by("-date_created")
            users = User.objects.filter(id__in=posts.values('author'))

            #import pdb
            #pdb.set_trace()
            #userId = User.objects.filter(email=request.data['email'])

            
            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        else:   
            posts = Post.objects.filter(is_question=False,category=category).order_by('-date_created')
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)

       

        return Response({
            'posts':serializer.data,
            'users':uSerializer.data
                
                
        })


#view that checks if all is the category and serailzer the data and returns a response
class GetAllQuestionsByCategoryView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get(self, request, category):
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=True).select_related("author").order_by("-date_created")
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        else:   
            posts = Post.objects.filter(is_question=True,category=category).order_by('-date_created')
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        return Response({
            'posts': serializer.data,
            'users':uSerializer.data
                
                
        })

    def post(self, request, category):
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=True).select_related("author").order_by("-date_created")
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        else:   
            posts = Post.objects.filter(is_question=True,category=category).order_by('-date_created')
            users = User.objects.filter(id__in=posts.values('author'))

            serializer = PostSerializer(posts, many=True)
            uSerializer = UserWithIdSerializer(users, many=True)
        return Response({
            'posts': serializer.data,
            'users':uSerializer.data
                      
        })







