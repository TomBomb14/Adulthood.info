from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
import posts.views

urlpatterns = [
    #post creation url that uses post creation view
        url(_(r'^PostCreation/$'),
            posts.views.PostCreationView.as_view(),
            name='PostCreation'),
            #gets a single post by pk
        url(_(r'^post/(?P<pk>\d+)$'),
            posts.views.GetPostView.as_view(),
            name='GetPost'),
            #deletes a single post by pk
        url(_(r'^PostDelete/(?P<id>[0-9]+)$'),
            posts.views.PostDeleteView.as_view(),
            name='DeletePost'),
            #updates a single post by pk
        url(_(r'^PostSave/(?P<id>[0-9]+)$'),
            posts.views.PostUpdateView.as_view(),
            name='UpdatePost'),
            #url mapping for getting all posts
        url(_(r'^GetAllPosts/$'),
            posts.views.GetAllPostsView.as_view(),
            name='GetPosts'),
            #get all questions
        url(_(r'^GetAllQuestions/$'),
            posts.views.GetAllQuestionsView.as_view(),
            name='GetQuestions'),
            #url mapping for getting all posts by category
        url(_(r'^GetAllPostsByCategory/(?P<category>\w{1,50})$'),
            posts.views.GetAllPostsByCategoryView.as_view(),
            name='GetPostsByCategory'),
            #same as above but questions
        url(_(r'^GetAllQuestionsByCategory/(?P<category>\w{1,50})$'),
            posts.views.GetAllQuestionsByCategoryView.as_view(),
            name='GetQuestionsByCategory'),
]
