export const SERVER_URL = 'http://localhost:8000'

// config should use named export as there can be different exports,
// just need to export default also because of eslint rules
export { SERVER_URL as default }

export const QUESTIONS = 0
export const POSTS = 1
export const MY_QUESTIONS = 2
export const MY_POSTS = 3
export const MY_COMMENTS = 4
