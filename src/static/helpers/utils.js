import { QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'

export function checkHttpStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  }

  const error = new Error(response.statusText)
  error.response = response
  throw error
}

export function parseJSON (response) {
  return response.json()
}

export function checkToken () {
  if (!getLocalToken()) return false
  else return true
}

export function getLocalToken () {
  return localStorage.getItem('token')
}

export function setLocalToken (token) {
  localStorage.setItem('token', token)
}

export function unsetLocalToken () {
  localStorage.removeItem('token')
}

export function formatDate (date) {
  var monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'July',
    'August', 'September', 'October',
    'November', 'December'
  ]
  const dateObj = new Date(date * 1000)
  var day = dateObj.getDate()
  var monthIndex = dateObj.getMonth()
  var year = dateObj.getFullYear()

  return day + ' ' + monthNames[monthIndex] + ' ' + year
}

export function getUrlForTabAndCategory (tab, category) {
  var url
  switch (tab) {
    case QUESTIONS:
      url = `/questions/category/${category}`
      break
    case POSTS:
      url = `/posts/category/${category}`
      break
    case MY_QUESTIONS:
      url = `/questions/category/${category}/authored`
      break
    case MY_POSTS:
      url = `/posts/category/${category}/authored`
      break
    case MY_COMMENTS:
      url = `/comments/category/${category}/authored`
      break
  }

  return url
}
