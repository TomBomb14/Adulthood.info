
import { CALL_API, getJSON } from 'redux-api-middleware'
import { SERVER_URL } from 'helpers/config'
import { getLocalToken } from 'helpers/utils'
import _ from 'lodash'
import { REQUEST_POSTS, RECEIVE_ALL_POSTS, REQUEST_ONE_POST, RECEIVE_ONE_POST, REQUEST_COMMENTS, DELETED_COMMENT, FAILURE_COMMENTS, FAILURE_POSTS, RECEIVE_USER_COMMENTS } from '../constants'

function requestCreateComment (content, postId) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/comments/CreateComment/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        content: content,
        post_id: postId
      }),
      types: [
        REQUEST_ONE_POST,
        RECEIVE_ONE_POST,
        FAILURE_POSTS
      ]
    }
  }
}

export function createComment (content, postId) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestCreateComment(content, postId)))
  }
}

function requestSaveComment (content, id) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/comments/UpdateComment/${id}`,
      method: 'PUT',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        content: content
      }),
      types: [
        REQUEST_ONE_POST,
        RECEIVE_ONE_POST,
        FAILURE_POSTS
      ]
    }
  }
}

export function saveComment (content, id) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestSaveComment(content, id)))
  }
}

function requestDeleteComment (id) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/comments/DeleteComment/${id}`,
      method: 'DELETE',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json'
      },
      types: [
        REQUEST_COMMENTS,
        DELETED_COMMENT,
        // {
        //   type: DELETED_COMMENT,
        //   payload: {
        //     deleted_id: id
        //   }
        // },
        FAILURE_COMMENTS
      ]
    }
  }
}

export function deleteComment (id) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestDeleteComment(id)))
  }
}

const initialState = {
  isFetching: false,
  error: '',
  commentIds: [],
  userCommentIds: [],
  statusText: '',
  status: 200
}

function comment (commentsArray) {
  var comments = {}
  commentsArray.forEach(comment => {
    comments[comment.id] = comment
  })
  return comments
}

export default function comments (state = initialState, action) {
  switch (action.type) {
    case REQUEST_COMMENTS:
      return {
        ...state,
        isFetching: true
      }
    case RECEIVE_ALL_POSTS:
    case RECEIVE_ONE_POST:
      return action.payload && action.payload.comments
    ? {
      ...state,
      ...comment(action.payload.comments),
      commentIds: _.union(action.payload.comments.map(comment => comment.id), state.commentIds)
    } : state
    case RECEIVE_USER_COMMENTS:
      return action.payload && action.payload.comments
      ? {
        ...state,
        ...comment(action.payload.comments),
        commentIds: _.union(action.payload.comments.map(comment => comment.id), state.commentIds),
        userCommentIds: action.payload.comments.map(comment => comment.id)
      } : state
    case DELETED_COMMENT:
      return {
        ...state,
        isFetching: false,
        [action.payload.id]: undefined,
        commentIds: state.commentIds.filter(id => id !== action.payload.id),
        status: 200
      }
    case FAILURE_COMMENTS:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error deleting comments ${action.payload.status} - ${action.payload.message}`
      }
    default :
      return state
  }
}
