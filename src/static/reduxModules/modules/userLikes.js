import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'
import { SERVER_URL, QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'  
import { getLocalToken } from 'helpers/utils'


import { ADD_USER_POST_LIKES, LIKES_POST_CREATION_SUCCESS, LIKES_POST_CREATION_FAILURE,
		DELETE_POST_USER_LIKES, LIKES_POST_DELETE_SUCCESS, LIKES_POST_DELETE_FAILURE,
    ADD_USER_COMMENT_LIKES, LIKES_COMMENT_CREATION_SUCCESS, LIKES_COMMENT_CREATION_FAILURE,
    DELETE_COMMENT_USER_LIKES, LIKES_COMMENT_DELETE_SUCCESS, LIKES_COMMENT_DELETE_FAILURE } from '../constants'


function requestPostSaveLike (userId, postId) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/likes/post/create/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userId,
        post_id: postId
        
      }),
      types: [
        ADD_USER_POST_LIKES,
        LIKES_POST_CREATION_SUCCESS,
        LIKES_POST_CREATION_FAILURE
      ]
    }
  }
}

export function savePostLikes (userId, postId) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestPostSaveLike(userId, postId)))
  }
}


function requestPostDeleteLike (userId, postId) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/likes/post/delete/`,
      method: 'DELETE',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userId,
        post_id: postId
        
      }),
      types: [
        DELETE_POST_USER_LIKES,
        LIKES_POST_DELETE_SUCCESS,
        LIKES_POST_DELETE_FAILURE
      ]
    }
  }
}

export function deletePostLikes (userId, postId) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestPostDeleteLike(userId, postId)))
  }
}


function requestCommentSaveLikes (userId, replyId) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/likes/comment/create/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userId,
        reply_id: replyId
        
      }),
      types: [
        ADD_USER_COMMENT_LIKES,
        LIKES_COMMENT_CREATION_SUCCESS,
        LIKES_COMMENT_CREATION_FAILURE
      ]
    }
  }
}

export function saveCommentLikes (userId, replyId) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestCommentSaveLikes(userId, replyId)))
  }
}



function requestCommentDeleteLikes (userId, commentId) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/likes/comment/delete/`,
      method: 'DELETE',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userId,
        reply_id: commentId
        
      }),
      types: [
        DELETE_COMMENT_USER_LIKES,
        LIKES_COMMENT_DELETE_SUCCESS,
        LIKES_COMMENT_DELETE_FAILURE
      ]
    }
  }
}

export function deleteCommentLikes (userId, replyId) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestCommentDeleteLikes(userId, replyId)))
 } 
}



// initial state
const initialState = {
  isSaved: '',
  isSaving: '',
  isDeleted: '',
  isDeleting: '',
  error: '',
  statusText: '',
  status: '',
  postsLikeIds: [],
  commentsLikeIds: []
}

// Reducers
export default function likes (state = initialState, action) {
  switch (action.type) {
    case ADD_USER_POST_LIKES:
      return {
        ...state,
        isSaving: true
      }
    case LIKES_POST_CREATION_SUCCESS:
      return action.payload ? !action.payload.success
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isSaving: false,
          isSaved: true,
          postsLikeIds: action.payload.posts_likes_ids,
          statusText: action.payload.success,
          status: 200
        } : {
          ...state,
          isSaving: false,
          isSaved: false,
          error: 'Something went wrong when saving post likes',
          status: 404
        }
   	case LIKES_POST_CREATION_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFetchingOne: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error fetching posts ${action.payload.status} - ${action.payload.message}`
      }
    case DELETE_POST_USER_LIKES:
      return {
        ...state,
        isSaving: true
      }
    case LIKES_POST_DELETE_SUCCESS:
      return action.payload ? !action.payload.success
        ? {
          ...state,
          isDeleting: false,
          error: ''
        }
        : {
          ...state,
          isDeleting: false,
          isDeleted: true,
          error: '',
          postsLikeIds: action.payload.posts_likes_ids,
          statusText: action.payload.success,
          status: 200
        } : {
          ...state,
          isSaving: false,
          isSaved: false,
          error: 'Something went wrong when deleting post likes',
          status: 404
        }
    case LIKES_POST_DELETE_FAILURE:
      return {
        ...state,
        isDeleting: false,
        isDeleted: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error deleting likes ${action.payload.status} - ${action.payload.message}`
      }
    case ADD_USER_COMMENT_LIKES:
      return {
        ...state,
        isSaving: true
      }
    case LIKES_COMMENT_CREATION_SUCCESS:
      return action.payload ? !action.payload.success
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isSaving: false,
          isSaved: true,
          commentsLikeIds: action.payload.comments_likes_ids,
          statusText: action.payload.success,
          status: 200
        } : {
          ...state,
          isSaving: false,
          isSaved: false,
          error: 'Something went wrong when saving comment likes',
          status: 404
        }
    case LIKES_COMMENT_CREATION_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFetchingOne: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error fetching posts ${action.payload.status} - ${action.payload.message}`
      }
     case DELETE_COMMENT_USER_LIKES:
      return {
        ...state,
        isSaving: true
      }
    case LIKES_COMMENT_DELETE_SUCCESS:
      return action.payload ? !action.payload.success
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isDeleting: false,
          isDeleted: true,
          error: '',
          commentsLikeIds: action.payload.comments_likes_ids,
          statusText: action.payload.success,
          status: 200
        } : {
          ...state,
          isDeleting: false,
          isDeleted: false,
          error: 'Something went wrong when deleting comment likes',
          status: 404
        }
    case LIKES_COMMENT_DELETE_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFetchingOne: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error fetching posts ${action.payload.status} - ${action.payload.message}`
      }
    default :
      return state
  }
}



