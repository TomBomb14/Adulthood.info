
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'
import { SERVER_URL, QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'
import { getLocalToken } from 'helpers/utils'
// constants
import { REQUEST_POSTS, FAILURE_POSTS,
  RECEIVE_ALL_POSTS, RECEIVE_ONE_POST, POST_CREATION_SUCCESS, DELETED_POST, REQUEST_SAVE_POSTS, RECEIVE_SAVED_POST, FAILURE_SAVE_POST, REQUEST_ONE_POST, DELETED_COMMENT, RECEIVE_USER_POSTS, REQUEST_USER_POSTS, FAILURE_USER_POSTS, REQUEST_USER_COMMENTS, RECEIVE_USER_COMMENTS, FAILURE_USER_COMMENTS } from '../constants'

// Action creators

export function fetchPost (id) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/posts/post/${id}`,
      method: 'GET',
      types: [
        REQUEST_ONE_POST,
        RECEIVE_ONE_POST,
        FAILURE_POSTS
      ]
    }
  }
}

export function getPost (id) {
  return function (dispatch) {
    return Promise.resolve(dispatch(fetchPost(id)))
  }
}

export function getPosts (tab, category, email) {

  const base = `${SERVER_URL}/api/v1`
  var url
  var request = REQUEST_POSTS
  var success = RECEIVE_ALL_POSTS
  var failure = FAILURE_POSTS

  switch (tab) {
    case QUESTIONS:
      url = `${base}/posts/GetAllQuestionsByCategory/${category}`
      break
    case POSTS:
      url = `${base}/posts/GetAllPostsByCategory/${category}`
      break
    case MY_QUESTIONS:
      url = `${base}/accounts/UserQuestions/${category}`
      request = REQUEST_USER_POSTS
      success = RECEIVE_USER_POSTS
      failure = FAILURE_USER_POSTS
        break
    case MY_POSTS:
      url = `${base}/accounts/UserPosts/${category}`
      request = REQUEST_USER_POSTS
      success = RECEIVE_USER_POSTS
      failure = FAILURE_USER_POSTS
      break
    case MY_COMMENTS:
      url = `${base}/accounts/UserComments/${category}`
      request = REQUEST_USER_COMMENTS
      success = RECEIVE_USER_COMMENTS
      failure = FAILURE_USER_COMMENTS
      break
  }

  // Email is true, return POST
  if (email) {

      return {
        [CALL_API]: {
          endpoint: url,
          method: 'POST',
          credentials: 'include',
          headers: {
            Authorization: `Token ${getLocalToken()}`,
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: email
          }),
          types: [
            request,
            success,
            failure
          ]
        }
      } //return 
  } else {

    //
    return {
      [CALL_API]: {
        endpoint: url,
        method: 'GET',
        credentials: 'include',
        headers: {
          Authorization: `Token ${getLocalToken()}`,
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        types: [
          request,
          success,
          failure
        ]
      }
    } //return
  } 
} //getPost

function requestCreatePost (title, category, content, isQuestion) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/posts/PostCreation/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: title,
        category: category,
        content: content,
        is_question: isQuestion
      }),
      types: [
        REQUEST_POSTS,
        POST_CREATION_SUCCESS,
        FAILURE_POSTS
      ]
    }
  }
}

export function createPost (title, category, content, isQuestion) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestCreatePost(title, category, content, isQuestion)))
  }
}

function requestSavePost (title, category, content, isQuestion, id) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/posts/PostSave/${id}`,
      method: 'PUT',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: title,
        category: category,
        content: content,
        is_question: isQuestion,
        author: '' // because we are using PUT
      }),
      types: [
        REQUEST_SAVE_POSTS,
        RECEIVE_SAVED_POST,
        FAILURE_SAVE_POST
      ]
    }
  }
}

export function savePost (title, category, content, isQuestion, id) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestSavePost(title, category, content, isQuestion, id)))
  }
}

function requestDeletePost (id) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/posts/PostDelete/${id}`,
      method: 'DELETE',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json'
      },
      types: [
        REQUEST_POSTS,
        {
          type: DELETED_POST,
          payload: {
            deleted_id: id
          }
        },
        FAILURE_POSTS
      ]
    }
  }
}

export function deletePost (id) {
  return function (dispatch) {
    return Promise.resolve(dispatch(requestDeletePost(id)))
  }
}

// initial state

const initialState = {
  isFetchingOne: true,
  isFetching: true,
  error: '',
  postIds: [],
  statusText: '',
  status: 200
}

function post (postsArray, comments) {
  var posts = {}
  postsArray.forEach(post => {
    posts[post.id] = post
    posts[post.id].is_question = posts[post.id].is_question === 'True'
    if (comments) {
      posts[post.id].commentIds = comments.map(comment => comment.id)
    } else if (!post.comments) {
      posts[post.id].commentIds = []
    } else {
      posts[post.id].commentIds = post.comments
    }
  })
  return posts
}

// Reducers
export default function posts (state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTS:
    case REQUEST_SAVE_POSTS:
    case REQUEST_USER_POSTS:
    case REQUEST_USER_COMMENTS:
      return {
        ...state,
        isFetching: true
      }
    case REQUEST_ONE_POST:
      return {
        ...state,
        isFetchingOne: true
      }
    case DELETED_POST:
      return {
        ...state,
        isFetching: false,
        [action.payload.deleted_id]: undefined,
        postIds: state.postIds.filter(id => id !== action.payload.deleted_id),
        status: 200
      }
    case RECEIVE_ONE_POST:
      return action.payload ? !action.payload.posts
      ? {
        ...state,
        isFetchingOne: false,
        error: ''
      }
      : {
        ...state,
        isFetchingOne: false,
        error: '',
        ...post(action.payload.posts, action.payload.comments),
        postIds: _.union(action.payload.posts.map(post => post.id), state.postIds),
        statusText: '',
        status: 200
      } : {
        ...state,
        isFetchingOne: false,
        error: 'Something went wrong when fetching',
        status: 404
      }
    case RECEIVE_ALL_POSTS:
    case POST_CREATION_SUCCESS:
    case RECEIVE_SAVED_POST:
    case RECEIVE_USER_POSTS:
    case RECEIVE_USER_COMMENTS:
      return action.payload ? !action.payload.posts
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...post(action.payload.posts, action.payload.comments),
          postIds: _.union(action.payload.posts.map(post => post.id), state.postIds),
          statusText: '',
          status: 200
        } : {
          ...state,
          isFetching: false,
          error: 'Something went wrong when fetching',
          status: 404
        }
    case DELETED_COMMENT:
      return {
        ...state,
        [action.payload.post_id]: {
          ...state[action.payload.post_id],
          commentIds: state[action.payload.post_id].commentIds.filter(id => id !== action.payload.id)
        }
      }
    case FAILURE_POSTS:
    case FAILURE_SAVE_POST:
    case FAILURE_USER_POSTS:
    case FAILURE_USER_COMMENTS:
      return {
        ...state,
        isFetching: false,
        isFetchingOne: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error fetching posts ${action.payload.status} - ${action.payload.message}`
      }
    default :
      return state
  }
}
