
import { CALL_API, getJSON } from 'redux-api-middleware'
// import _ from 'lodash'
import { SERVER_URL } from 'helpers/config'

// constants
import { REQUEST_CATEGORIES, FAILURE_CATEGORIES,
  RECEIVE_CATEGORIES } from '../constants'

// initial state
const initialState = {
  isFetching: false,
  error: '',
  categories: [],
  statusText: ''
}

export function getCategories (areQuestions, category) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/category/list`,
      method: 'GET',
      types: [
        REQUEST_CATEGORIES,
        RECEIVE_CATEGORIES,
        FAILURE_CATEGORIES
      ]
    }
  }
}

// Reducers
export default function lookups (state = initialState, action) {
  switch (action.type) {
    case REQUEST_CATEGORIES:
      return {
        ...state,
        isFetching: true
      }
    case RECEIVE_CATEGORIES:
      return {
        ...state,
        isFetching: false,
        categories: action.payload,
        statusText: '',
        status: 200,
        error: ''
      }
    case FAILURE_CATEGORIES:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Error fetching posts ${action.payload.status} - ${action.payload.message}`
      }
    default :
      return state
  }
}
