import { SELECT_TAB, SELECT_CATEGORY } from '../constants'
import { QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'
// Action creators
export function selectCategory (category) {
  return {
    type: SELECT_CATEGORY,
    payload: {
      category
    }
  }
}

export function selectTab (tab) {
  return {
    type: SELECT_TAB,
    payload: {
      tab
    }
  }
}
// initial state

const initialState = {
  activeCategory: 'all',
  activeTab: POSTS
}
// Reducers
export default function posts (state = initialState, action) {
  switch (action.type) {
    case SELECT_CATEGORY:
      return {
        ...state,
        activeCategory: action.payload.category
      }
    case SELECT_TAB:
      return {
        ...state,
        activeTab: action.payload.tab
      }
    default :
      return state
  }
}
