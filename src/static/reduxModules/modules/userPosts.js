import { getLocalToken } from 'helpers/utils'
import { schema, normalize } from 'normalizr'
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'

// constants
import {REQUEST_USER_POSTS, RECEIVE_USER_POSTS, FAILURE_USER_POSTS} from '../constants'

const nodeSchema = new schema.Entity('nodes')
const nodesSchema = new schema.Entity('userNodes',
  { nodes: [nodeSchema]
  }
  )

// Action creators

export function getUserPosts (id) {
  return {
    [CALL_API]: {
      endpoint: `userPosts/${id}`,
      method: 'GET',
      headers: { 'Authorization': `Bearer ${getLocalToken()}` },
      types: [
        REQUEST_USER_POSTS,
        {
          type: RECEIVE_USER_POSTS,
          payload: (action, state, res) => {
            return getJSON(res).then((json) => {
              return normalize(json, nodesSchema)
            })
          }
        },
        FAILURE_USER_POSTS
      ]
    }
  }
}

// initial state

const initialState = {
  isFetching: true,
  error: '',
  postIds: []
}

// Reducers
export default function userPosts (state = initialState, action) {
  switch (action.type) {
    case REQUEST_USER_POSTS:
      return {
        ...state,
        isFetching: true
      }
    case RECEIVE_USER_POSTS:
      return action.payload ? !action.payload.posts
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          postIds: _.union(action.payload.posts.map(post => post.id), state.postIds)
        }
        : {
          ...state,
          isFetchingOne: false,
          error: 'Something went wrong when fetching',
          status: 404
        }
    case FAILURE_USER_POSTS:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message
      }
    default :
      return state
  }
}
