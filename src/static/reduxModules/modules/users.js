// stores user info
import { SERVER_URL } from 'helpers/config'
import { getLocalToken, setLocalToken, unsetLocalToken, checkHttpStatus, parseJSON } from 'helpers/utils'
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'
// import jwtDecode from 'jwt-decode'

import { REQUEST_POSTS, RECEIVE_ALL_POSTS, RECEIVE_ONE_POST, FAILURE_POSTS, UNAUTH_USER,
  REMOVE_FETCHING_ACTIVE_USER, REQUEST_ACTIVE_USER, RECEIVE_ACTIVE_USER,
  FAILURE_ACTIVE_USER, REQUEST_REGISTER, RECEIVE_REGISTER, FAILURE_REGISTER, 
  FAILURE_ACTIVE_USER_TOKEN, POST_CREATION_SUCCESS, RECEIVE_USER_POSTS, SAVE_USER_SUCCESS, SAVE_USER_FAILURE, CLEAR_USER_SAVE_STATUS, REQUEST_SAVE, RECEIVE_USER_COMMENTS } from '../constants'
// Action creators

export function logoutAndUnauth () {
  unsetLocalToken()
  return {
    type: UNAUTH_USER
  }
}

export function removeFetchingActiveUser () {
  return {
    type: REMOVE_FETCHING_ACTIVE_USER
  }
}

function requestActiveUser () {
  return {
    type: REQUEST_ACTIVE_USER
  }
}

function authLoginUserFailure (status, message) {
  return {
    type: FAILURE_ACTIVE_USER,
    payload: {
      status,
      message
    }
  }
}

function authLoginUserSuccess (token, user) {
  return {
    type: RECEIVE_ACTIVE_USER,
    payload: {
      token,
      user
    }
  }
}

export function loginWithCredentials (email, password) {
  return (dispatch) => {
    dispatch(requestActiveUser())
    const auth = btoa(`${email}:${password}`)
    return fetch(`${SERVER_URL}/api/v1/accounts/login/`, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${auth}`
      }
    })
          .then(checkHttpStatus)
          .then(parseJSON)
          .then((response) => {
            setLocalToken(response.token)
            dispatch(authLoginUserSuccess(response.token, response.user))
          })
          .catch((error) => {
            if (error && typeof error.response !== 'undefined' && error.response.status === 401) {
                  // Invalid authentication credentials
              return error.response.json().then((data) => {
                dispatch(authLoginUserFailure(401, data.non_field_errors[0]))
              })
            } else if (error && typeof error.response !== 'undefined' && error.response.status >= 500) {
                  // Server side error
              dispatch(authLoginUserFailure(500, 'A server error occurred while sending your data!'))
            } else {
                  // Most likely connection issues
              dispatch(authLoginUserFailure('Connection Error', 'An error occurred while sending your data!'))
            }

            return Promise.resolve()
          })
  }
}
export function register (email, password, fname, lname) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/accounts/register/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        first_name: fname,
        last_name: lname,
        email: email,
        password: password
      }),
      types: [REQUEST_REGISTER, {
        type: RECEIVE_REGISTER,
        payload: (action, state, res) => {
          const result = getJSON(res).then((data) => {
            setLocalToken(data.token)
            return data
          })
          return result
        }
      }, FAILURE_REGISTER]
    }
  }
}


function fetchActiveUser () {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/accounts/loginWithToken/`,
      method: 'GET',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json'},
      types: [REQUEST_ACTIVE_USER, {
        type: RECEIVE_ACTIVE_USER,
        payload: (action, state, res) => {
          const result = getJSON(res).then((data) => {
            // setLocalToken(data.token)
            return data
          })
          return result
        }
      }, FAILURE_ACTIVE_USER_TOKEN]
    }
  }
}

export function getActiveUser () {
  return function (dispatch) {
    return Promise.resolve(dispatch(fetchActiveUser()))
  }
}
export function clearSaveStatus() {
  return {
    type: CLEAR_USER_SAVE_STATUS
  }
}

function updateUser (email, password, fname, lname) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/accounts/UpdateUser/`,
      method: 'PUT',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json',
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          first_name: fname,
          last_name: lname,
          email: email,
          password: password
      }),
      types: [REQUEST_SAVE, {
        type: SAVE_USER_SUCCESS,
        payload: (action, state, res) => {
          const result = getJSON(res).then((data) => {
            return data
          })
          return result
        }
      }, SAVE_USER_FAILURE]
    }
  }
}

export function saveUser (email, password, fname, lname) {
  return function (dispatch) {
    return Promise.resolve(dispatch(updateUser(email, password, fname, lname)))
  }
}
// initial state

const initialState = {
  isFetching: true,
  isFetchingActive: false,
  isSaving: false,
  error: '',
  isAuthed: false,
  authedEmail: '',
  emails: [],
  statusText: '',
}

// Reducers

function user (userArray) {
  var users = {}
  userArray.forEach(user => {
    users[user.email] = user
  })

  return users
}

export default function users (state = initialState, action) {
  switch (action.type) {
    case UNAUTH_USER :
      return {
        ...state,
        isAuthed: false,
        authedEmail: '',
        statusText: ''
      }
    case REQUEST_ACTIVE_USER:
    case REQUEST_REGISTER:
      return {
        ...state,
        isFetchingActive: true
      }
    case REMOVE_FETCHING_ACTIVE_USER :
      return {
        ...state,
        isFetchingActive: false
      }
    case RECEIVE_ACTIVE_USER:
      return action.payload ? !action.payload.user
        ? {
          ...state,
          isFetchingActive: false,
          error: '',
          statusText: `Authentication Error: ${action.payload.status} - ${action.payload.statusText}`
        }
        : {
          ...state,
          isFetchingActive: false,
          error: '',
          [action.payload.user.email]: action.payload.user,
          isAuthed: true,
          authedEmail: action.payload.user.email,
          emails: _.union([action.payload.user.email], state.emails),
          statusText: 'You have been successfully logged in.'
        } : {

        }
    case RECEIVE_REGISTER:
      return action.payload.user ? {
        ...state,
        isFetchingActive: false,
        error: '',
        [action.payload.user.email]: action.payload.user,
        isAuthed: true,
        authedEmail: action.payload.user.email,
        emails: _.union([action.payload.user.email], state.emails),
        statusText: 'You have been successfully logged in.'
      } : {
        ...state,
        isFetchingActive: false
      }
    case REQUEST_SAVE:
      return {
        ...state,
        isSaving: true
      }
    case SAVE_USER_SUCCESS:
      return action.payload.users ? {
        ...state,
        isSaving: false,
        statusText: 'Your changes have been saved successfully',
        error: '',
        [action.payload.users[0].email]: action.payload.users[0],
        authedEmail: action.payload.users[0].email,
        emails: _.union([action.payload.users[0].email], state.emails),
      } : {
        ...state,
        isSaving: false,
      }
    case FAILURE_ACTIVE_USER:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Authentication Error: ${action.payload.status} - ${action.payload.message}`
      }
    case SAVE_USER_FAILURE:
      return {
        ...state,
        isSaving: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Save Error: ${action.payload.status} - ${action.payload.message}`
      }
    case FAILURE_ACTIVE_USER_TOKEN:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: ''
      }
      case FAILURE_REGISTER:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Registration Error: ${action.payload.response.email}`
      }
      case RECEIVE_ALL_POSTS:
      case RECEIVE_ONE_POST:
      case POST_CREATION_SUCCESS:
      case RECEIVE_USER_POSTS:
      case RECEIVE_USER_COMMENTS:
        return action.payload ? !action.payload.users
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...user(action.payload.users),
          emails: _.union(action.payload.users.map(user => user.email), state.emails)
        } : {
          ...state,
          isFetching: false,
          error: 'Something went wrong when fetching'
        }
      default :
      return state
    }
  }
  
