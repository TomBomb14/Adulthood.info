// users
export const UNAUTH_USER = 'UNAUTH_USER'
// const REMOVE_FETCHING_USER = 'REMOVE_FETCHING_USER'
export const REMOVE_FETCHING_ACTIVE_USER = 'REMOVE_FETCHING_ACTIVE_USER'
// const REQUEST_USER = '/user/REQUEST'
export const REQUEST_ACTIVE_USER = '/user/REQUEST_ACTIVE'
export const FAILURE_ACTIVE_USER = '/user/FAILURE_ACTIVE'
export const REQUEST_REGISTER = '/user/REQUEST_REGISTER'
export const RECEIVE_REGISTER = '/user/RECEIVE_REGISTER'
export const FAILURE_REGISTER = '/user/FAILURE_REGISTER'
export const FAILURE_ACTIVE_USER_TOKEN = '/user/FAILURE_ACTIVE_USER_TOKEN'
export const SAVE_USER_SUCCESS = '/user/SAVE_SUCCESS'
export const REQUEST_SAVE = '/user/REQUEST_SAVE'
export const SAVE_USER_FAILURE = '/user/SAVE_FAILURE'
export const CLEAR_USER_SAVE_STATUS = '/user/CLEAR_SAVE_STATUS'
// const RECEIVE_USER = '/user/RECEIVE'
export const RECEIVE_ACTIVE_USER = '/user/RECEIVE_ACTIVE'
export const FAILURE_USER = '/user/FAILURE'

// posts
export const REQUEST_POSTS = '/posts/REQUEST'
export const RECEIVE_POSTS = '/posts/RECEIVE'
export const FAILURE_POSTS = '/posts/FAILURE'
export const RECEIVE_ALL_POSTS = '/posts/RECEIVE_ALL'
export const POST_CREATION_SUCCESS = '/posts/CREATION_SUCCESS'
export const DELETED_POST = '/posts/DELETED_POST'
export const FAILURE_SAVE_POST = '/posts/FAILURE_SAVE'
export const RECEIVE_SAVED_POST = '/posts/RECEIVE_SAVED'
export const RECEIVE_ONE_POST = '/posts/RECEIVE_ONE'
export const REQUEST_ONE_POST = '/posts/REQUEST_ONE'
export const REQUEST_SAVE_POSTS = '/posts/REQUEST_SAVE'

// user posts
export const REQUEST_USER_POSTS = '/userPosts/REQUEST'
export const RECEIVE_USER_POSTS = '/userPosts/RECEIVE'
export const FAILURE_USER_POSTS = '/userPosts/FAILURE'
export const ADD_USER_POST = '/userPosts/ADD'

// lookups
export const REQUEST_CATEGORIES = '/lookups/REQUEST_CATEGORIES'
export const FAILURE_CATEGORIES = '/lookups/FAILURE_CATEGORIES'
export const RECEIVE_CATEGORIES = '/lookups/RECEIVE_CATEGORIES'

// comments
export const REQUEST_COMMENTS = '/comments/REQUEST'
export const DELETED_COMMENT = '/comments/DELETED_COMMENT'
export const FAILURE_COMMENTS = '/comments/FAILURE'

// user comments
export const REQUEST_USER_COMMENTS = '/userComments/REQUEST'
export const RECEIVE_USER_COMMENTS = '/userComments/RECEIVE'
export const FAILURE_USER_COMMENTS = '/userComments/FAILURE'

// nav
export const SELECT_CATEGORY = '/nav/SELECT_CATEGORY'
export const SELECT_TAB = '/nav/SELECT_TAB'


// likes
export const REQUEST_USER_POST_LIKES = '/likes/post/REQUEST'
export const RECEIVE_USER_POST_LIKES = '/likes/post/RECEIVE'
export const FAILURE_USER_POST_LIKES = '/likes/post/FAILURE'
// userLikes NOTE: As a prototype use one redux file called likes.js
export const ADD_USER_POST_LIKES = '/likes/post/ADD'
export const LIKES_POST_CREATION_SUCCESS = '/likes/post/CREATION_SUCCESS'
export const LIKES_POST_CREATION_FAILURE = '/likes/post/CREATION_FAILURE'
export const DELETE_POST_USER_LIKES = '/likes/post/DELETE'
export const LIKES_POST_DELETE_SUCCESS = '/likes/post/DELETE_SUCCESS'
export const LIKES_POST_DELETE_FAILURE = '/likes/post/DELETE_FAILURE'
//comment likes
export const ADD_USER_COMMENT_LIKES = '/likes/comment/ADD'
export const LIKES_COMMENT_CREATION_SUCCESS = '/likes/comment/CREATION_SUCCESS'
export const LIKES_COMMENT_CREATION_FAILURE = '/likes/comment/CREATION_FAILURE'
export const DELETE_COMMENT_USER_LIKES = '/likes/comment/DELETE'
export const LIKES_COMMENT_DELETE_SUCCESS = '/likes/comment/DELETE_SUCCESS'
export const LIKES_COMMENT_DELETE_FAILURE = '/likes/comment/DELETE_FAILURE'

