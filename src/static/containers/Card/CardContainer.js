import React, { Component } from 'react'
import { bgWhite, cardBorder, hoverCardBorder } from './cardStyle.scss'
import PropTypes from 'prop-types'

export default class CardContainer extends Component {
  static propTypes = {
    onClick: PropTypes.func,
    hover: PropTypes.bool
  }
  render () {
    return (
      <div className={this.props.hover ? hoverCardBorder : cardBorder} onClick={this.props.onClick}>
        <div className={bgWhite}>
          {this.props.children}
        </div>
      </div>
    )
  }
}
