import React from 'react'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import * as postActionCreators from 'reduxModules/modules/posts'
import * as navActionCreators from 'reduxModules/modules/nav'
import * as likeActionCreators from 'reduxModules/modules/userLikes'

import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { Home, Loading, ErrorMessage } from 'components'

// tabs
import { QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'
import { getUrlForTabAndCategory } from 'helpers/utils'
class HomeContainer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  
    if(props.users.isAuthed) {
      if(this.props.userLikes.postsLikeIds.length !== 0) { 
        this.props.users[this.props.users.authedEmail].post_likes = this.props.userLikes.postsLikeIds.slice(0)
      }
      if(this.props.userLikes.commentsLikeIds !== 0) {
        this.props.users[this.props.users.authedEmail].comment_likes = this.props.userLikes.commentsLikeIds.slice(0)
      }
    } 
  }

  componentDidMount () {

    const newState = this.update(this.props)
    // fetch posts or questions
    this.props.selectTab(newState.tab)
    this.props.selectCategory(newState.category)
    if (this.props.users.isAuthed) {
       this.setState({ 
        postsLiked: this.props.users[this.props.users.authedEmail].post_likes
      })
      this.setState({ 
        commentsLiked: this.props.users[this.props.users.authedEmail].comment_likes
      })
      this.props.getPosts(newState.tab, newState.category, this.props.users.authedEmail)
    } else {
      this.props.getPosts(newState.tab, newState.category)
    }
  }

  componentWillReceiveProps (nextProps) {
    const newState = this.update(nextProps)

    if (nextProps.match.url !== this.props.match.url) {
      this.props.selectTab(newState.tab)
      this.props.selectCategory(newState.category)

      if (this.props.users.isAuthed) {
          this.setState({ 
            postsLiked: this.props.users[this.props.users.authedEmail].post_likes
          })
          this.setState({ 
            commentsLiked: this.props.users[this.props.users.authedEmail].comment_likes
          })
          this.props.getPosts(newState.tab, newState.category, nextProps.users.authedEmail)
      } else {
          this.props.getPosts(newState.tab, newState.category)
      }
    }
  }
  

  update = (props) => {
    // match category and tab from url
    if (props.match.params.category) {
      const areAuthored = props.match.path.indexOf('authored') !== -1
      const areQuestions = props.match.path.indexOf('questions') !== -1
      const areComments = props.match.path.indexOf('comments') !== -1
      const arePosts = props.match.path.indexOf('posts') !== -1
      var tab
      if (areAuthored) {
        tab = areComments ? MY_COMMENTS : areQuestions ? MY_QUESTIONS : arePosts ? MY_POSTS : MY_POSTS
      } else {
        tab = areQuestions ? QUESTIONS : POSTS
      }
      return { category: props.match.params.category, tab: tab, props: props }
    } else {
      return { category: 'all', tab: POSTS, props: props }
    }
  }

  goToProtected = () => {
    this.props.push('/protected')
  }

  goToPost = (id) => {
    this.props.push(`/posts/${id}`)
  }

  switchTab = (tab) => {
    this.props.selectTab(tab)
    this.props.push(getUrlForTabAndCategory(tab, this.props.activeCategory))
  }

  render () {
    var displayPosts = {}
    const category = this.props.activeCategory
    const tab = this.props.activeTab
    const isNotUserPosts = tab === QUESTIONS || tab === POSTS
    const isComments = tab === MY_COMMENTS
    const ids = isNotUserPosts ? this.props.posts.postIds : !isComments ? this.props.userPosts.postIds : this.props.userCommentIds
    const obj = isComments ? this.props.comments : this.props.posts

    ids.forEach(id => {
      const thisPostCategoryIsSameAsActiveOrAll = obj[id].category === category || category === 'all'

      if (thisPostCategoryIsSameAsActiveOrAll) {
        if (isComments) {
          displayPosts[id] = obj[id]
        } else {
          const thisPostIsQuestion = obj[id].is_question
          const thisTabIsQuestion = tab === QUESTIONS || tab === MY_QUESTIONS
          if (thisPostIsQuestion === thisTabIsQuestion) {
            displayPosts[id] = obj[id]
          }
        }
      }
    })

    return this.props.isFetching ? <Loading />
      : this.props.status === 200 ? (
        <Home
          userName={this.props.userName}
          goToProtected={this.goToProtected}
          posts={isComments ? this.props.posts : displayPosts}
          comments={isComments ? displayPosts : null}
          users={this.props.users}
          goToPost={this.goToPost}
          tab={tab}
          switchTab={this.switchTab}
          postsLiked={this.props.users.isAuthed ? this.props.users[this.props.users.authedEmail].post_likes : []}
          commentsLiked={this.props.users.isAuthed ? this.props.users[this.props.users.authedEmail].comment_likes : []}
          isAuthed={this.props.isAuthed} />
      ) : <ErrorMessage message="This category doesn't exist" />
  }
}

const mapStateToProps = ({users, posts, userPosts, nav, comments, userLikes}) => {
  return {
    userName: users.authedEmail ? users[users.authedEmail].first_name : '',
    posts,
    users,
    userLikes: userLikes,
    isFetching: posts.isFetching,
    status: posts.status,
    isAuthed: users.isAuthed,
    userPosts,
    activeTab: nav.activeTab,
    activeCategory: nav.activeCategory,
    comments,
    userCommentIds: comments.userCommentIds
  }
}

HomeContainer.propTypes = {
  push: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }),
  // users
  users: PropTypes.object.isRequired,
  userName: PropTypes.string,
  // posts
  getPosts: PropTypes.func.isRequired,
  status: PropTypes.number.isRequired,
  posts: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  // user posts
  userPosts: PropTypes.object.isRequired,
  // comments
  comments: PropTypes.object.isRequired,
  // user comments
  userCommentIds: PropTypes.array.isRequired,
  // nav
  selectTab: PropTypes.func.isRequired,
  userLikes: PropTypes.object.isRequired,
  selectCategory: PropTypes.func.isRequired,
  activeCategory: PropTypes.string.isRequired,
  activeTab: PropTypes.number.isRequired
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...postActionCreators,
    ...navActionCreators,
    ...likeActionCreators,
    push
  }, dispatch)
)(HomeContainer)
export { HomeContainer as HomeContainerNotConnected }
