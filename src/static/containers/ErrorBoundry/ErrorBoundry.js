import React from 'react'
import { app, coverContainer } from 'sharedStyles/shared.scss'
import { ErrorMessage } from 'components'
export default class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch (error, info) {
    // Display fallback UI
    this.setState({ hasError: true })
    console.log(error, info)
  }

  render () {
    if (this.state.hasError) {
      return (
        <div className={this.props.background ? app : coverContainer}>
          <ErrorMessage message='Something went wrong' />
        </div>

      )
    }
    return this.props.children
  }
}
