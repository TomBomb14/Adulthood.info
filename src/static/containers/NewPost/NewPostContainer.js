import React from 'react'
import { PropTypes } from 'prop-types'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import * as postActionCreators from 'reduxModules/modules/posts'
import * as navActionCreators from 'reduxModules/modules/nav'
import { bindActionCreators } from 'redux'
import { NewPost, Loading } from 'components'
import { PleaseLoginContainer } from 'containers'

class NewPostContainer extends React.Component {
  constructor (props) {
    super(props)
    if (props.match && props.match.path === '/new') {
      this.state = {
        formValues: {
          title: '',
          category: '',
          content: '',
          isQuestion: false
        },
        error: 'All fields are required',
        editing: false
      }
    } else {
      const id = props.postId
      if (!props.posts[id] && !props.isFetchingPost) {
        props.getPost(id)
      } else if (props.posts[id]) {
        this.setInitialForId(id)
      } else {
        this.state = {
          formValues: {
            title: '',
            category: '',
            content: '',
            isQuestion: false
          },
          error: 'All fields are required',
          editing: true
        }
      }
    }
  }

  setInitialForId = (id) => {
    const post = this.props.posts[id]
    this.state = {
      formValues: {
        title: post.title,
        category: post.category,
        content: post.content,
        isQuestion: post.is_question
      },
      error: '',
      editing: true,
      postId: id
    }
  }

  submit = () => {
    const values = this.state.formValues

    if (values) {
      if (this.state.error === '') {
        if (this.state.editing) {
          /* One of many instances that an UPDATE REQUEST only has a few attributes that are sent.
           * To avoid sending the whole object, check which attributes are being edited and only send those attributes at the back. Django will take care of everything in terms of updating the db schema.
           *
           *
           * #NOTE:
           *    For now we are doing a PUT call, and in the future we have to do a PATCH call.
           *
           * #STEPS for PATCH:
           *    Check the original snapshot of the store
           *    Iterate through the store and if the snapshot is different, append to the new object to send to the backend
           * */
          this.props.savePost(values.title, values.category, values.content,
                                values.isQuestion, this.state.postId)
          .then(() => {
            if (this.props.error === '') {
              this.props.exitEditingPost()
            }
          })
        } else {
          // create a new post
          this.props.createPost(values.title, values.category, values.content, values.isQuestion)
          .then(() => {
            if (this.props.error === '') {
              this.props.push(`/posts/${this.props.lastPostId}`)
            }
          })
        }
      }
    }
  }
  deletePost = (e) => {
    if (this.state.editing) {
      // delete the post
      this.props.deletePost(this.state.postId)
      .then(() => {
        if (this.props.error === '') {
          this.props.push('/posts/category/all')
          this.props.selectCategory('all')
        }
      })
    }
  }

  onFormChange = (value) => {
    this.setState({ formValues: value })
    this.validate(value)
  }

  validate = (value) => {
    if (value.title === '') {
      this.setState({ error: 'Title is required' })
    } else {
      if (value.category === '') {
        this.setState({ error: 'Category is required' })
      } else {
        if (value.content === '') {
          this.setState({ error: 'Content is required' })
        } else this.setState({ error: '' })
      }
    }
  }

  render () {
    return (
      <div>
        {this.props.isAuthed ? (!this.props.isFetchingPost ? <NewPost
          formValues={this.state.formValues}
          statusText={this.props.statusText}
          submit={this.submit}
          onFormChange={this.onFormChange}
          error={this.state.error}
          categories={this.props.categories}
          editing={this.state.editing}
          deletePost={this.deletePost}
          exitEditingPost={this.props.exitEditingPost} /> : <Loading />)
        : <PleaseLoginContainer location={this.props.location} message='create or edit a post or a question' />
        }
      </div>
    )
  }
}

NewPostContainer.propTypes = {
  statusText: PropTypes.string.isRequired,
  isAuthed: PropTypes.bool.isRequired,
  createPost: PropTypes.func.isRequired,
  savePost: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  posts: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  lastPostId: PropTypes.string,
  isFetchingPost: PropTypes.bool.isRequired,
  deletePost: PropTypes.func.isRequired,
  selectCategory: PropTypes.func.isRequired,
  exitEditingPost: PropTypes.func
}

const mapStateToProps = ({users, posts, lookups}) => {
  return {
    statusText: posts.statusText,
    isAuthed: users.isAuthed,
    lastPostId: posts.postIds[0],
    error: posts.error,
    categories: lookups.categories,
    posts: posts,
    isFetchingPost: posts.isFetching
  }
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...postActionCreators,
    ...navActionCreators,
    push
  }, dispatch)
)(NewPostContainer)
export { NewPostContainer as NewPostContainerNotConnected }
