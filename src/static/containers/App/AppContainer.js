import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as userActionCreators from 'reduxModules/modules/users'
import * as postActionCreators from 'reduxModules/modules/posts'
import * as lookupsActionCreators from 'reduxModules/modules/lookups'
import * as navActionCreators from 'reduxModules/modules/nav'
import { bindActionCreators } from 'redux'
// import { authLogoutAndRedirect } from './actions/auth'
import 'resources/styles/main.scss'
import { Navbar, Menu, Loading } from 'components'
import { checkToken, getUrlForTabAndCategory } from 'helpers/utils'
import { mainContainer, contentContainer, categoryBackdrop, footer } from './appStyle.scss'
import { app } from 'sharedStyles/shared.scss'
import { ErrorBoundry } from 'containers'
import { MY_COMMENTS, POSTS } from 'helpers/config'

class AppContainer extends React.Component {
  static propTypes = {
    children: PropTypes.shape().isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }),
        // users
    isAuthed: PropTypes.bool.isRequired,
    isFetchingActive: PropTypes.bool.isRequired,
    logoutAndUnauth: PropTypes.func.isRequired,
    user: PropTypes.object,
    // nav
    selectCategory: PropTypes.func.isRequired,
    selectTab: PropTypes.func.isRequired,
    activeCategory: PropTypes.string.isRequired,
    activeTab: PropTypes.number.isRequired,
    // lookups
    getCategories: PropTypes.func.isRequired,
    isFetchingCategory: PropTypes.bool.isRequired
  }

  static defaultProps = {
    location: undefined
  }

  componentWillMount () {
    this.checkAuth()
    this.props.getCategories()
  }

  checkAuth = () => {
    if (!this.props.isAuthed && !this.props.error && checkToken()) {
             // logging in with token
      this.props.getActiveUser()
    }
  }

  logout = () => {
    this.goToIndex()
    if (this.props.activeTab === MY_COMMENTS) {
      this.props.selectTab(POSTS)
    }
    this.props.logoutAndUnauth()
  }

  goToIndex = () => {
    this.props.push('/')
  }

  goToLogin = () => {
    this.props.push('/login')
  }

  goToUser = () => {
    this.props.push('/user')
  }

  goToNew = () => {
    this.props.push('/new')
  }

  goToCategory = (category) => {
    this.props.selectCategory(category)
    this.props.push(getUrlForTabAndCategory(this.props.activeTab, category))
  }

  getCategory = () => {
    return this.props.activeCategory
  }

  render () {
    const homeClass = classNames({
      active: this.props.location && this.props.location.pathname === '/'
    })
    const protectedClass = classNames({
      active: this.props.location && this.props.location.pathname === '/protected'
    })
    const loginClass = classNames({
      active: this.props.location && this.props.location.pathname === '/login'
    })

    const activeCategory = this.getCategory()

    return (
      <div className={app}>
        <Navbar
          homeClass={homeClass}
          loginClass={loginClass}
          protectedClass={protectedClass}
          goToIndex={this.goToIndex}
          goToProtected={this.goToProtected}
          logout={this.logout}
          goToLogin={this.goToLogin}
          goToUser={this.goToUser}
          isAuthed={this.props.isAuthed}
          user={this.props.user} />
        <div className={mainContainer}>
          {this.props.isFetchingCategory ? null : <Menu goToCategory={this.goToCategory} goToNew={this.goToNew} categories={this.props.categories} activeCategory={activeCategory} />}
          <ErrorBoundry>
            <div className={contentContainer}>
              {this.props.isFetchingActive ? <Loading /> : this.props.children}
            </div>
          </ErrorBoundry>
        </div>
        <div className={categoryBackdrop}>{activeCategory}</div>
        <footer className={footer}> Contact us at adulthood.info@gmail.com © 2017</footer>
      </div>
    )
  }
}

const mapStateToProps = ({users, routing, lookups, posts, nav}, ownProps) => {
  const user = users.isAuthed ? users[users.authedEmail] : null
  return {
    isAuthed: users.isAuthed,
    location: routing.location,
    isFetchingActive: users.isFetchingActive,
    error: users.error,
    user: user,
    categories: ['all', ...lookups.categories],
    activeCategory: nav.activeCategory,
    activeTab: nav.activeTab,
    isFetchingCategory: lookups.isFetching
  }
}

export default connect(
    mapStateToProps,
    (dispatch) => bindActionCreators(
      {
        ...userActionCreators,
        ...postActionCreators,
        ...lookupsActionCreators,
        ...navActionCreators,
        push
      },
         dispatch
      )
)(AppContainer)
export { AppContainer as AppContainerNotConnected }
