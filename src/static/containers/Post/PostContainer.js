import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as userActionCreators from 'reduxModules/modules/users'
import * as postActionCreators from 'reduxModules/modules/posts'
import * as navActionCreators from 'reduxModules/modules/nav'
import * as likeActionCreators from 'reduxModules/modules/userLikes'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Post, Loading, ErrorMessage, Comment } from 'components'
import { NewCommentContainer, NewPostContainer } from 'containers'
import { Route, Switch } from 'react-router'

class PostContainer extends Component {
  static propTypes = {
    isFetchingPost: PropTypes.bool.isRequired,
    users: PropTypes.object.isRequired,
    posts: PropTypes.object.isRequired,
    getPost: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }),
    status: PropTypes.number.isRequired,
    comments: PropTypes.object.isRequired,
    // nav
    selectCategory: PropTypes.func.isRequired,
    activeCategory: PropTypes.string
  }
  constructor (props) {
    super(props)
    this.state = { 
      postLiked: false,
      commentLiked: false,
      postsLiked: [],
      commentsLiked: [], 
      editingCommentId: '', 
      isEditingPost: false, 
      isFetchingComments: false 
    }

    // props.getPost(props.match.params.id)

    //Do the following if user is Authed:
    // 1.) Set the postLiked to true
    // 2.) Naive copy the users liked comments into the commentsLiked Array
    
    props.getPost(props.match.params.id)

  }

  componentDidMount () {
   //Do the following if user is Authed:
    // 1.) Set the postLiked to true
    // 2.) Naive copy the users liked comments into the commentsLiked Array

    if(this.props.users.isAuthed) {
      
      if(this.props.users[this.props.users.authedEmail].post_likes.includes(this.props.match.params.id) ) {
        this.setState({postLiked: true})
      }
      if(this.props.users[this.props.users.authedEmail].comment_likes.includes(this.props.match.params.id) ) {
        this.setState({commentLiked: true})
      }
      this.state.postsLiked =  this.props.users[this.props.users.authedEmail].post_likes.slice(0)
      this.state.commentsLiked = this.props.users[this.props.users.authedEmail].comment_likes.slice(0)

    }


  }

  componentWillUpdate () {
    // this.update(this.props)
  }

  componentWillReceiveProps = (nextProps) => {

    const post = nextProps.posts[nextProps.match.params.id]
    // if the category of the post is not the same as active one, update active
    if (post && post.category !== this.props.activeCategory) {
      this.props.selectCategory(post.category)
    } 
    // if their are updates on the like 

  }//componentWillReceiveProps

  update = (props) => {
    const postId = props.match.params.id
    if (!props.isFetchingPost && props.status === 200) {
        // if post is not in redux, fetch the post
        
        props.getPost(postId)
    }
  }

  editPost = (e) => {
    e.preventDefault()
    this.setState({isEditingPost: true})
    // this.props.push(`/posts/edit/${this.props.match.params.id}`)
  }

  // NOTE: LIKE OR UNLIKE a post, question or comment
  like = (post, commentId) => {
    //Check if user is authed
    // Reroute to login if user is not login
    if(!this.props.users.isAuthed) {
      const redirectAfterLogin = this.props.location.pathname
      this.props.push(`/login?next=${redirectAfterLogin}`)

    } else {

      if (commentId === undefined) {
        // Steps:
        // Check if their is a commentId:
        //   If yes, go to else statement to:
        //   Else, save or delete liked post:

        if (this.state.postLiked) {
          //Steps:
            // Check if the postIsLiked
            //  Yes, deletePostLikes
            //  -1 the number of likes
            //  postLiked === false
          this.props.deletePostLikes(this.props.users.authedEmail, post.id)
          .then(this.updatePostLike(post))
        } else {
            //Steps:
            // Check if the postIsLiked
            //  No, createPostLikes
            //  +1 the number of likes
            //  postLiked === true
            this.props.savePostLikes(this.props.users.authedEmail, post.id)
            .then(this.updatePostLike(post))
        }

      } else {
        //Steps:
            // Check if the commentIsLiked
            //  Yes, deleteCommentLikes
            //  -1 the number of likes
            //  commentLiked === false
        //Check if this is liked
        // If yes take out the likes in the array
        // Else, add new to array 
        const index = this.state.commentsLiked.indexOf(commentId)

        if (index !== -1) {
          this.props.deleteCommentLikes(this.props.users.authedEmail, commentId)
          .then(this.updateCommentLike(post, commentId, index))
        } else {
          this.props.saveCommentLikes(this.props.users.authedEmail, commentId)
          .then(this.updateCommentLike(post, commentId, index))
        }
       
      }//else
    }//isAuthed 
  }//liked()

  updatePostLike = (post) => {

    if (this.state.postLiked) {
      let index = this.state.postsLiked.indexOf(post.id)
      if (index !== -1) {
        let ar = this.state.postsLiked
        ar.splice(index, 1)
        this.setState({postsLiked: ar})
      }
      post.number_of_likes -= 1
      
      //Check if the store changes for the new user
    } else {

      this.setState({postsLiked: [...this.state.postsLiked, post.id]})
      post.number_of_likes += 1

    }
    this.setState({postLiked: !this.state.postLiked})


  }

  updateCommentLike = (post, commentId, index) => {
        if (index !== -1) {
          var ar = this.state.commentsLiked
          ar.splice(index, 1)
          this.setState({commentsLiked: ar})
          post.number_of_likes -= 1
        } else {
           //Steps:
            // Check if the commentIsLiked
            //  No, createCommentLikes
            //  +1 the number of likes
            //  commentLiked === true
          this.setState({commentsLiked: [...this.state.commentsLiked, commentId]})
          post.number_of_likes += 1

        }//else
        this.setState({commentLiked: !this.state.commentLiked})
  }


  // comment a post or question or reply to a comment
  comment = (post) => {
    this.setState({editingCommentId: 'new'})
    this.scroll()
  }

  scroll = () => {
    if (!this.props.isFetchingPost) {
      this.newCommentRef.scrollIntoView({ behavior: 'smooth' })
    }
  }

  exitEditingComment = () => {
    this.setState({editingCommentId: ''})
  }

  exitEditingPost = () => {
    this.setState({isEditingPost: false})
  }

  editComment = (id) => {
    this.setState({editingCommentId: id})
  }
  checkCommentsReady = (post, comments) => {
    var res = true
    post.commentIds.every(commentId => {
      const comment = comments[commentId]
      if (!comment) {
        res = false
        return false
      } else return true
    })
    return res
  }
  render () {
    //NOTE: 
    // Check to see if state is liked
    // User is changed with the new array
    const post = this.props.posts[this.props.match.params.id]
    const comments = this.props.comments
    return this.props.isFetchingPost ? <Loading />
        : post
        ? <div>
          { this.state.isEditingPost
          ? <NewPostContainer
            postId={this.props.match.params.id}
            exitEditingPost={this.exitEditingPost} />
            : <Post
              post={post}
              users={this.props.users}
              editPost={this.editPost}
              like={() => this.like(post)}
              comment={() => this.comment(post)}
              isLiked={this.state.postLiked} />
          }
          {this.checkCommentsReady(post, comments) ? post.commentIds.map(commentId => {
            const comment = comments[commentId]
            return <div key={commentId}>
              {this.state.editingCommentId === commentId
              ? <NewCommentContainer
                commentId={commentId}
                postId={post.id}
                exitEditingComment={this.exitEditingComment} />
              : <Comment
                comment={comment}
                authedEmail={this.props.users.authedEmail}
                author={this.props.users[comment.author]}
                like={() => this.like(comment, commentId)}
                reply={() => this.comment(comment)}
                isLiked={this.state.commentsLiked.indexOf(commentId) !== -1}
                edit={() => this.editComment(commentId)} /> }
            </div>
          }) : null}
          <div ref={(el) => { this.newCommentRef = el }}>
            {this.state.editingCommentId === 'new'
            ? <NewCommentContainer
              postId={this.props.match.params.id}
              exitEditingComment={this.exitEditingComment} />
            : null}
          </div>

        </div>
        : (this.props.status === 200) ? <Loading /> : <ErrorMessage message="This post doesn't exist" />
  }
}

const mapStateToProps = ({users, posts, comments, routing, nav, userLikes}) => {
  return {
    isFetchingPost: posts.isFetchingOne,
    users: users,
    posts: posts,
    userLikes: userLikes,
    location: routing.location,
    status: posts.status,
    comments: comments,
    activeCategory: nav.activeCategory
  }
}

export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...userActionCreators,
    ...postActionCreators,
    ...navActionCreators,
    ...likeActionCreators,
    push
  }, dispatch)
)(PostContainer)
export { PostContainer as PostContainerNotConnected }
