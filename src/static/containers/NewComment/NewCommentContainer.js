import React from 'react'
import { PropTypes } from 'prop-types'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import * as commentActionCreators from 'reduxModules/modules/comments'
import * as postActionCreators from 'reduxModules/modules/posts'
import * as navActionCreators from 'reduxModules/modules/nav'
import { bindActionCreators } from 'redux'
import { NewComment, Loading } from 'components'
import { PleaseLoginContainer } from 'containers'

class NewCommentContainer extends React.Component {
  constructor (props) {
    super(props)
    const postId = props.postId

    if (!props.commentId) {
      this.state = {
        formValues: {
          content: ''
        },
        error: 'All fields are required',
        postId: postId,
        editing: false
      }
    } else {
      const commentId = props.commentId

      if (props.comments[commentId]) {
        this.setInitialForId(postId, commentId)
      } else {
        this.state = {
          formValues: {
            content: ''
          },
          error: 'All fields are required',
          editing: true,
          postId: postId
        }
      }
    }
  }

  static propTypes = {
    createComment: PropTypes.func.isRequired,
    saveComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    statusText: PropTypes.string.isRequired,
    isAuthed: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    isFetchingPost: PropTypes.bool.isRequired,
    exitEditingComment: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }),
    postId: PropTypes.string
  }

  componentDidMount () {
    this.scroll()
  }

  scroll = () => {
    if (!this.props.isFetchingPost) {
      this.formRef.scrollIntoView({ behavior: 'smooth' })
    }
  }

  setInitialForId = (postId, commentId) => {
    const comment = this.props.comments[commentId]
    this.state = {
      formValues: {
        content: comment.content
      },
      error: '',
      editing: true,
      postId: postId,
      commentId: commentId
    }
  }

  submit = () => {
    const values = this.state.formValues

    if (values) {
      if (this.state.error === '') {
        // save edited comment
        if (this.state.editing) {
          this.props.saveComment(values.content, this.state.commentId)
          .then(this.goBackToPost)
        } else {
          // create a new comment
          this.props.createComment(values.content, this.props.postId)
          .then(this.goBackToPost)
        }
      }
    }
  }

  deleteComment = (e) => {
    if (this.state.editing) {
      // delete the comment
      this.props.deleteComment(this.state.commentId)
      .then(this.goBackToPost)
    }
  }

  goBackToPost = () => {
    if (this.props.error === '') {
      this.props.exitEditingComment()
    }
  }

  onFormChange = (value) => {
    this.setState({ formValues: value })
    this.validate(value)
  }

  validate = (value) => {
    if (value.content === '') {
      this.setState({ error: 'Content is required' })
    } else {
      this.setState({ error: '' })
    }
  }

  cancelComment = () => {
    this.props.exitEditingComment()
  }

  render () {
    return (
      <div ref={(el) => { this.formRef = el }}>
        {this.props.isAuthed
          ? (!this.props.isFetchingPost
            ? <div ><NewComment
              formValues={this.state.formValues}
              statusText={this.props.statusText}
              submit={this.submit}
              onFormChange={this.onFormChange}
              error={this.state.error}
              editing={this.state.editing}
              deleteComment={this.deleteComment}
              cancelComment={this.cancelComment} /></div>
            : <Loading />)
          : <PleaseLoginContainer location={this.props.location} message='create or edit a comment' />}
      </div>
    )
  }
}

const mapStateToProps = ({users, comments, posts, routing}) => {
  return {
    comments,
    posts,
    statusText: posts.statusText,
    isAuthed: users.isAuthed,
    error: posts.error,
    isFetchingPost: posts.isFetching,
    location: routing.location
  }
}
export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...commentActionCreators,
    ...postActionCreators,
    ...navActionCreators,
    push
  }, dispatch)
)(NewCommentContainer)
export { NewCommentContainer as NewCommentContainerNotConnected }
