import React from 'react'
import { ErrorMessage } from 'components'
export default class NotFoundContainer extends React.Component {
  render () {
    return <ErrorMessage message='404 Page Not Found :(' />
  }
}
