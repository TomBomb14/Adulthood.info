import React from 'react'
import { PropTypes } from 'prop-types'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import * as userActionCreators from 'reduxModules/modules/users'
import { bindActionCreators } from 'redux'
import { UserProfile } from 'components'

class UserProfileContainer extends React.Component {
  constructor (props) {
    super(props)
    const user = this.props.user
    this.state = {
      formValues: {
        email: user.email,
        newPassword: '',
        repeatNewPassword: '',
        firstName: user.first_name,
        lastName: user.last_name
      },
      status: '',
      error: ''
    }
  }
  static propTypes = {
    user: PropTypes.object.isRequired,
    saveUser: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    statusText: PropTypes.string.isRequired,
    isSaving: PropTypes.bool.isRequired
  }

  onFormChange = (value) => {
    this.setState({ formValues: value, status: '' })
    this.validate(value)
  }

  validate = (value) => {
    if (value.newPassword !== value.repeatNewPassword) {
      this.setState({ error: 'passwords don\'t match' })
    } else {
      if (value.firstName === '') {
        this.setState({ error: 'First name is required' })
      } else {
        if (value.lastName === '') {
          this.setState({ error: 'Last name is required' })
        } else this.setState({ error: '' })
      }
    }
  }

  submitUser = () => {
    const values = this.state.formValues
    if (values) {
      if (this.state.error === '') {
        this.props.saveUser(values.email, values.newPassword, values.firstName, values.lastName)
        .then(() => {
          this.setState({ status: this.props.statusText })
        })
      }
    }
  }

  render () {
    return (
      <div>
        <UserProfile
          user={this.props.user}
          statusText={this.state.status}
          submit={this.submitUser}
          formValues={this.state.formValues}
          onFormChange={this.onFormChange}
          error={this.state.error}
          isSaving={this.props.isSaving} />
      </div>
    )
  }
}
const mapStateToProps = ({users}) => {
  return {
    user: users[users.authedEmail],
    statusText: users.statusText,
    error: users.error,
    isSaving: users.isSaving
  }
}
export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...userActionCreators,
    push
  }, dispatch)
)(UserProfileContainer)
export { UserProfileContainer as UserProfileContainerNotConnected }
