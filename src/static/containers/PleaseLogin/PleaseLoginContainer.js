
import React from 'react'
import { PropTypes } from 'prop-types'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import * as userActionCreators from 'reduxModules/modules/users'
import { bindActionCreators } from 'redux'
import { title, marginTop30 } from 'sharedStyles/shared.scss'
import { CardContainer } from 'containers'

class PleaseLoginContainer extends React.Component {
  login = () => {
    const redirectAfterLogin = this.props.location.pathname
    this.props.push(`/login?next=${redirectAfterLogin}`)
  }
  render () {
    return (
      <div>
        <div className='container'>
          <div className={marginTop30}>
            <CardContainer>
              <h1 className={title}>Please <a className={title} onClick={this.login}>login</a> to {this.props.message ? this.props.message : 'access this page'}</h1>
            </CardContainer>
          </div>
        </div>
      </div>
    )
  }
}
PleaseLoginContainer.propTypes = {
  isAuthed: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  message: PropTypes.string
}
const mapStateToProps = (state) => {
  return {
    isAuthed: state.users.isAuthed
  }
}
export default connect(
  mapStateToProps,
  (dispatch) => bindActionCreators({
    ...userActionCreators,
    push
  }, dispatch)
)(PleaseLoginContainer)
export { PleaseLoginContainer as PleaseLoginContainerNotConnected }
