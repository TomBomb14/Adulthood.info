import React from 'react'
import { PropTypes } from 'prop-types'
import { title, marginTop30, cancelBtn } from 'sharedStyles/shared.scss'
import t from 'tcomb-form'
import classNames from 'classnames'
import { CardContainer } from 'containers'

const NewComment = ({statusText, submit,
  formValues, onFormChange, error, editing, deleteComment, cancelComment}) => {
  const Form = t.form.Form

  const FormStruct = t.struct({
    content: t.String
  })

  const FormOptions = {
    auto: 'placeholders',
    hasError: error !== '',
    fields: {
      content: {
        type: 'textarea',
        attrs: {
          rows: 10
        }
      }
    },
    error: <i>{error}</i>
  }

  let status = null
  if (statusText) {
    const statusTextClassNames = classNames({
      'alert': true,
      'alert-danger': statusText.indexOf('Error') !== -1,
      'alert-success': statusText.indexOf('Error') === -1
    })

    status = (
      <div className='row'>
        <div className='col-sm-12'>
          <div className={statusTextClassNames}>
            {statusText}
          </div>
        </div>
      </div>
              )
  }
  return (
    <div className='container'>
      <div >
        <CardContainer>
          <span className={cancelBtn} onClick={cancelComment}>&times;</span>
          <h1 className={title}>{editing ? 'Editing ' : 'Create new '}{'comment'}</h1>
          <div className='margin-top-medium'>
            {status}
            <form onSubmit={(e) => {
              e.preventDefault()
              submit()
            }}>
              <Form
                type={FormStruct}
                options={FormOptions}
                value={formValues}
                onChange={onFormChange}
              />
              <button disabled={error !== ''}
                type='submit'
                className='btn btn-primary'
              >
                  Save
              </button>
              {editing ? <button type='button' style={{float: 'right'}} onClick={deleteComment} className='btn btn-danger '> Delete </button> : null}
            </form>
          </div>
        </CardContainer>
      </div>
    </div>
  )
}
NewComment.propTypes = {
  statusText: PropTypes.string.isRequired,
  formValues: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  onFormChange: PropTypes.func.isRequired,
  editing: PropTypes.bool.isRequired,
  deleteComment: PropTypes.func.isRequired,
  cancelComment: PropTypes.func.isRequired
}
export default NewComment
