import React from 'react'
import { PropTypes } from 'prop-types'
import { CardContainer } from 'containers'
import { LikeBar } from 'components'
import { formatDate } from 'helpers/utils'
import { name, date } from 'sharedStyles/shared.scss'
const Comment = ({comment, author, reply, like, isLiked, authedEmail, edit}) => (
  <div className='container'>
    <CardContainer>
      {authedEmail === author.email ? <button style={{float: 'right'}} className='btn btn-primary' onClick={edit}> Edit</button> : null}
      <p className={name}><i className='fa fa-user' /> {author.first_name}{' '}{author.last_name}</p>
      <p>{comment.content}</p>
      <p className={date}>{formatDate(comment.date_created)}</p>
      <LikeBar nLikes={Number(comment.number_of_likes)} nComments={0} like={like} comment={reply} isLiked={isLiked} />
    </CardContainer>
  </div>
)
Comment.propTypes = {
  comment: PropTypes.object.isRequired,
  author: PropTypes.object.isRequired,
  like: PropTypes.func.isRequired,
  reply: PropTypes.func.isRequired,
  isLiked: PropTypes.bool.isRequired,
  edit: PropTypes.func.isRequired,
  authedEmail: PropTypes.string.isRequired
}
export default Comment
