import React from 'react'
import { PropTypes } from 'prop-types'
import { title, name, date } from 'sharedStyles/shared.scss'
import { PostedBy, LikeBar } from 'components'
import { CardContainer } from 'containers'
import { formatDate } from 'helpers/utils'

const CommentCard = ({comment, user, post, author, goToPost, isLiked}) => (
  <div className='container' style={{'textAlign': 'left'}}>
    <CardContainer onClick={() => goToPost(post.id)} hover>
      <p className={name}><i className='fa fa-user' /> {user.first_name}{' '}{user.last_name}</p>
      <p>{comment.content}</p>
      <p className={date}>{formatDate(comment.date_created)}</p>
      <hr />
      <h3 className={title} style={{'fontSize': '22px'}}>{post.title} -> </h3>
      <LikeBar
        nLikes={Number(comment.number_of_likes)}
        nComments={0}
        like={() => {}}
        comment={() => {}}
        isLiked={isLiked} />
    </CardContainer>
  </div>
)
CommentCard.PropTypes = {
  post: PropTypes.object.isRequired,
  author: PropTypes.object.isRequired,
  goToPost: PropTypes.func.isRequired,
  comment: PropTypes.object.isRequired,
  isLiked: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired
}
export default CommentCard
