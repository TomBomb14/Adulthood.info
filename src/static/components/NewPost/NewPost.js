import React from 'react'
import { PropTypes } from 'prop-types'
import { title, marginTop30, cancelBtn } from 'sharedStyles/shared.scss'
import t from 'tcomb-form'
import classNames from 'classnames'
import { CardContainer } from 'containers'

const NewPost = ({statusText, submit,
  formValues, onFormChange, error, categories, editing, deletePost, exitEditingPost}) => {
  const Form = t.form.Form

  var categoryObj = {}
  categories.forEach(cat => {
    categoryObj[cat] = capitalize(cat)
  })

  function capitalize (str) {
    return str.slice(0, 1).toUpperCase() + str.slice(1)
  }
  const categoriesEnum = t.enums(categoryObj)

  const FormStruct = t.struct({
    title: t.String,
    category: categoriesEnum,
    content: t.String,
    isQuestion: t.Boolean
  })

  const FormOptions = {
    auto: 'placeholders',
    hasError: error !== '',
    fields: {
      content: {
        type: 'textarea',
        attrs: {
          rows: 10
        }
      },
      category: {
        nullOption: {value: '', text: 'Select category'}
      }
    },
    error: <i>{error}</i>
  }

  let status = null
  if (statusText) {
    const statusTextClassNames = classNames({
      'alert': true,
      'alert-danger': statusText.indexOf('Error') !== -1,
      'alert-success': statusText.indexOf('Error') === -1
    })

    status = (
      <div className='row'>
        <div className='col-sm-12'>
          <div className={statusTextClassNames}>
            {statusText}
          </div>
        </div>
      </div>
            )
  }
  return (

    <div className='container'>
      <div className={marginTop30}>
        <CardContainer>
          {editing ? <span className={cancelBtn} onClick={exitEditingPost}>&times;</span> : null}
          <h1 className={title}>{editing ? 'Editing ' : 'Create new '}{formValues.isQuestion ? 'question' : 'post'}</h1>
          <div className='margin-top-medium'>
            {status}
            <form onSubmit={(e) => {
              e.preventDefault()
              submit()
            }}>
              <Form
                type={FormStruct}
                options={FormOptions}
                value={formValues}
                onChange={onFormChange}
                />
              <button disabled={error !== ''}
                type='submit'
                className='btn btn-primary'
                >
                    Save
                </button>
              {editing ? <button type='button' style={{float: 'right'}} onClick={deletePost} className='btn btn-danger '> Delete </button> : null}
            </form>
          </div>
        </CardContainer>
      </div>
    </div>
  )
}
NewPost.propTypes = {
  statusText: PropTypes.string.isRequired,
  formValues: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  onFormChange: PropTypes.func.isRequired,
  categories: PropTypes.array.isRequired,
  editing: PropTypes.bool.isRequired,
  deletePost: PropTypes.func.isRequired,
  exitEditingPost: PropTypes.func
}
export default NewPost
