import React from 'react'
import { PropTypes } from 'prop-types'
import { PostCard, CommentCard } from 'components'
import classnames from 'classnames'
import { title, leftBtn, rightBtn, btn, activeBtn } from './homeStyle.scss'
// tabs
import { QUESTIONS, POSTS, MY_QUESTIONS, MY_POSTS, MY_COMMENTS } from 'helpers/config'
const Home = ({
  userName,
  goToProtected,
  posts,
  comments,
  users,
  goToPost,
  switchTab,
  like,
  postsLiked,
  commentsLiked,
  isAuthed,
  tab = POSTS
}) => (
  <div className='container'>
    <div className='margin-top-medium text-center' />
    <div className='text-center'>
      <button className={classnames(leftBtn, tab === QUESTIONS && activeBtn)}
        onClick={() => switchTab(QUESTIONS)}>
          Questions
        </button>
      <button className={classnames(isAuthed ? btn : rightBtn, tab === POSTS && activeBtn)}
        onClick={() => switchTab(POSTS)}>
          Posts
        </button>
      {isAuthed ? <span>
        <button className={classnames(btn, tab === MY_QUESTIONS && activeBtn)}
          onClick={() => switchTab(MY_QUESTIONS)}>
            My Questions
          </button>
        <button className={classnames(btn, tab === MY_POSTS && activeBtn)}
          onClick={() => switchTab(MY_POSTS)}>
            My Posts
          </button>
        <button className={classnames(rightBtn, tab === MY_COMMENTS && activeBtn)}
          onClick={() => switchTab(MY_COMMENTS)}>
            My Comments
          </button></span> : null
        }

      <h3 className={title}>
        {tab === QUESTIONS
            ? Object.keys(posts).length !== 0
              ? 'Here are the most recent questions'
              : 'There are no recent questions'
            : Object.keys(posts).length !== 0
              ? 'Here are the most recent posts'
              : 'There are no recent posts'}{' '}
      </h3>
    </div>
    <div className='margin-top-medium text-center'>
      {(tab === MY_COMMENTS)
      ? Object.keys(comments).reverse()
        .map(commentKey => <CommentCard
          key={commentKey}
          comment={comments[commentKey]}
          user={users[users.authedEmail]}
          post={posts[comments[commentKey].post_id]}
          author={users[posts[comments[commentKey].post_id].author]}
          goToPost={goToPost}
          isLiked={commentsLiked.indexOf(commentKey) !== -1}
        />)
        : Object.keys(posts)
          .reverse()
          .map(postKey => <PostCard
            key={postKey}
            post={posts[postKey]}
            author={users[posts[postKey].author]}
            goToPost={goToPost}
            like={() => { }}
            comment={() => { }}
            isLiked={postsLiked.indexOf(postKey) !== -1}
            />)
          }
    </div>
  </div>
  )
Home.PropTypes = {
  userName: PropTypes.string,
  goToProtected: PropTypes.func.isRequired,
  posts: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  goToPost: PropTypes.func.isRequired,
  tabs: PropTypes.number,
  switchTab: PropTypes.func.isRequired,
  postsLiked: PropTypes.func.isRequired,
  isAuthed: PropTypes.bool.isRequired
}
export default Home
