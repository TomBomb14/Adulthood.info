import React from 'react'
import { PropTypes } from 'prop-types'
import { title } from 'sharedStyles/shared.scss'
import { PostedBy, LikeBar } from 'components'
import { CardContainer } from 'containers'

const cutoff = 200

const PostCard = ({post, author, goToPost, comment, like, isLiked}) => (
  <CardContainer onClick={() => goToPost(post.id)} hover>
    <h2 className={title}>{post.title}</h2>
    <PostedBy post={post} author={author} />
    <p>{post.content.slice(0, cutoff)}{post.content.length > cutoff ? '...' : null}</p>
    <LikeBar
      nLikes={Number(post.number_of_likes)}
      nComments={post.commentIds.length}
      like={like}
      comment={comment}
      isLiked={isLiked} />
  </CardContainer>
)
PostCard.PropTypes = {
  post: PropTypes.object.isRequired,
  author: PropTypes.object.isRequired,
  goToPost: PropTypes.func.isRequired,
  like: PropTypes.func.isRequired,
  comment: PropTypes.func.isRequired,
  isLiked: PropTypes.bool.isRequired
}
export default PostCard
