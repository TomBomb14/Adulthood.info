import React from 'react'
import { PropTypes } from 'prop-types'

const Navbar = ({homeClass, loginClass, goToIndex,
  logout, goToLogin, goToUser, isAuthed, user}) => (
    <nav className='navbar navbar-default affix affix-top' style={{width: '100%', zIndex: 200}} data-offset-top='50'>
      <div className='container-fluid'>
        <div className='navbar-header'>
          <button type='button'
            className='navbar-toggle collapsed'
            data-toggle='collapse'
            data-target='#top-navbar'
            aria-expanded='false'
                >
            <span className='sr-only'>Toggle navigation</span>
            <span className='icon-bar' />
            <span className='icon-bar' />
            <span className='icon-bar' />
          </button>
          <a className='navbar-brand' onClick={goToIndex}>
                    Adulthood.info
                </a>
        </div>
        <div className='collapse navbar-collapse' id='top-navbar'>
          {isAuthed
                    ? <ul className='nav navbar-nav navbar-right'>
                      <li className={homeClass}>
                        <a className='js-go-to-index-button' onClick={goToIndex}>
                          <i className='fa fa-home' /> Home
                            </a>
                      </li>
                      <li>
                        <a className='js-logout-button' onClick={logout}>
                                Logout
                            </a>
                      </li>
                      <li className={loginClass}>
                        <a className='js-login-button' onClick={goToUser}>
                          <i className='fa fa-user' /> {user.first_name}{' '}{user.last_name}
                        </a>
                      </li>
                    </ul>
                    : <ul className='nav navbar-nav navbar-right'>
                      <li className={homeClass}>
                        <a className='js-go-to-index-button' onClick={goToIndex}>
                          <i className='fa fa-home' /> Home
                            </a>
                      </li>
                      <li className={loginClass}>
                        <a className='js-login-button' onClick={goToLogin}>
                          <i className='fa fa-home' /> Login
                            </a>
                      </li>
                    </ul>
                }
        </div>
      </div>
    </nav>
)

Navbar.propTypes = {
  homeClass: PropTypes.string.isRequired,
  loginClass: PropTypes.string.isRequired,
  user: PropTypes.object,
  isAuthed: PropTypes.bool.isRequired,
  goToLogin: PropTypes.func.isRequired,
  goToIndex: PropTypes.func.isRequired,
  goToUser: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
}

export default Navbar
