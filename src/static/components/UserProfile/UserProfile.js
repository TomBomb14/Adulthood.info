import React from 'react'
import { PropTypes } from 'prop-types'
import { CardContainer } from 'containers'
import classNames from 'classnames'
import { title, marginTop30 } from 'sharedStyles/shared.scss'
import t from 'tcomb-form'

const UserProfile = ({user, statusText, submit,
formValues, onFormChange, isSaving, error}) => {
  const Form = t.form.Form

  const LoginStruct = t.struct({
    email: t.String,
    firstName: t.String,
    lastName: t.String,
    newPassword: t.String,
    repeatNewPassword: t.String
  })

  const LoginFormOptions = {
    auto: 'placeholders',
    fields: {
      newPassword: {
        type: 'password'
      },
      repeatNewPassword: {
        type: 'password'
      },
      firstName: {
        type: 'string'
      },
      lastName: {
        type: 'string'
      }
    },
    hasError: error !== '',
    error: <i>{error}</i>
  }

  let status = null
  if (statusText) {
    const statusTextClassNames = classNames({
      'alert': true,
      'alert-danger': statusText.indexOf('Error') !== -1,
      'alert-success': statusText.indexOf('Error') === -1
    })

    status = (
      <div className='row'>
        <div className='col-sm-12'>
          <div className={statusTextClassNames}>
            {statusText}
          </div>
        </div>
      </div>
        )
  }

  return <div className='container login'>
    <div className={marginTop30}>
      <CardContainer>
        <h1 className={title}>{'User profile: '}{user.first_name}{' '}{user.last_name}</h1>
        <div className='login-container margin-top-medium'>
          {status}
          <form onSubmit={(e) => {
            e.preventDefault()
            submit()
          }}>
            <Form
              type={LoginStruct}
              options={LoginFormOptions}
              value={formValues}
              onChange={onFormChange}
                  />
            <button disabled={error !== '' || isSaving}
              type='submit'
              className='btn btn-default btn-block'>
                      Submit
            </button>
          </form>
        </div>
      </CardContainer>
    </div>
  </div>
}
UserProfile.propTypes = {
  user: PropTypes.object.isRequired,
  statusText: PropTypes.string.isRequired,
  formValues: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  onFormChange: PropTypes.func.isRequired,
  isSaving: PropTypes.bool.isRequired
}
export default UserProfile
