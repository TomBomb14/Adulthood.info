import React from 'react'
import { PropTypes } from 'prop-types'
import { title, marginTop30 } from 'sharedStyles/shared.scss'
import { PostedBy, LikeBar } from 'components'
import { CardContainer } from 'containers'
const Post = ({post, users, editPost, comment, like, isLiked}) => (
  <div className='container'>
    <div className={marginTop30}>
      <CardContainer>
        {post.author === users.authedEmail ? <button className='btn btn-primary' style={{float: 'right'}} onClick={editPost}>{'Edit'}</button> : null}
        <h2 className={title}>{post.title}</h2>
        <PostedBy post={post} author={users[post.author]} />
        <p>{post.content}</p>
        <LikeBar nLikes={Number(post.number_of_likes)} nComments={post.commentIds.length} like={like} comment={comment} isLiked={isLiked} />
      </CardContainer>
    </div>
  </div>
)
Post.propTypes = {
  post: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  editPost: PropTypes.func.isRequired,
  like: PropTypes.func.isRequired,
  comment: PropTypes.func.isRequired,
  isLiked: PropTypes.bool.isRequired
}
export default Post
