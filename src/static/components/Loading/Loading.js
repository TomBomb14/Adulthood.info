import React from 'react'
import { ErrorMessage } from 'components'
const Loading = (props) => (
  <ErrorMessage message='Loading...' />
)
export default Loading
