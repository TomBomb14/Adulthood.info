import React from 'react'
import { PropTypes } from 'prop-types'
import { postedBy, name, category } from './postedByStyle'
import { formatDate } from 'helpers/utils'

const PostedBy = ({author, post}) => (
  <p className={postedBy}>{'Posted by '}
    <span className={name}><i className='fa fa-user' /> {author.first_name} {author.last_name}</span>
    {' on '}
    <span>{formatDate(post.date_created)}</span>
    {' in '}
    <span className={category}>{post.category}</span>
  </p>
)
PostedBy.PropTypes = {
  author: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired
}
export default PostedBy
