import React from 'react'
import { PropTypes } from 'prop-types'
import {menuContainer, menuBtn, activeBtn, createNewBtn} from './menuStyle.scss'

// TODO: make collapsable
const Menu = ({goToNew, categories, goToCategory, activeCategory}) => (
  <div className={menuContainer}>
    <ul>
      {categories.map(category => <li key={category} ><button className={activeCategory === category ? activeBtn : menuBtn} onClick={() => goToCategory(category)}>{category}</button></li>)}
    </ul>
    <button className={createNewBtn} onClick={goToNew}>New post/question</button>
  </div>
)
Menu.PropTypes = {
  goToNew: PropTypes.func.isRequired,
  categories: PropTypes.array.isRequired,
  goToCategory: PropTypes.func.isRequired,
  activeCategory: PropTypes.string.isRequired
}
export default Menu
