import React from 'react'
import { PropTypes } from 'prop-types'
import { container, box } from './errorStyle.scss'
const ErrorMessage = ({message}) => (
  <div className={container}>
    <div className={box}>
      <h1 style={{color: 'white', textAlign: 'center'}}>{message}</h1>
    </div>
  </div>
)
ErrorMessage.propTypes = {
  message: PropTypes.string.isRequired
}
export default ErrorMessage
