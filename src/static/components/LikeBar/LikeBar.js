import React from 'react'
import { PropTypes } from 'prop-types'
import { likeBarContainer, text, btn } from './likeBarStyle.scss'
const btnSize = '25px'
const grey = '#D6D6D6'
const red = '#DB4437'

const getHeart = (liked) => <svg width={btnSize} height={btnSize} viewBox='0 0 22 20' version='1.1' xmlns='http://www.w3.org/2000/svg' >
  <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
    <g transform='translate(-5.000000, -2.000000)' fill={liked ? red : grey}id='heart'>
      <path d='M15.952381,22 C15.952381,22 17.6605216,20.561382 20.2380952,18.1904762 C22.8156689,15.8195704 26.9047619,11.7359848 26.9047619,7.71428571 C26.9047619,4.85714286 25,2 21.1904762,2 C17.3809524,2 15.952381,5.80952381 15.952381,5.80952381 C15.952381,5.80952381 14.5238095,2 10.7142857,2 C6.9047619,2 5,4.85714286 5,7.71428571 C5,11.7671641 8.76554751,15.5366734 11.6666667,18.1904762 C14.5677858,20.844279 15.952381,22 15.952381,22 Z' />
    </g>
  </g>
</svg>

const bubble = <svg className='svgIcon-use' width={btnSize} height={btnSize} viewBox='0 0 22 20' xmlns='http://www.w3.org/2000/svg' version='1.1'>
  <g transform='translate(-2, -4)' fillRule='evenodd'>
    <g id='bubble'>
      <path fill={grey} d='m21.27,20.058c1.89,-1.826 2.754,-4.17 2.754,-6.674c0,-5.174 -4.354,-9.384 -9.924,-9.384c-5.57,0 -10.1,4.21 -10.1,9.384c0,5.175 4.53,9.385 10.1,9.385c1.007,0 2,-0.14 2.95,-0.41c0.285,0.25 0.592,0.49 0.918,0.7c1.306,0.87 2.716,1.31 4.19,1.31c0.276,-0.01 0.494,-0.14 0.6,-0.36a0.625,0.625 0 0 0 -0.052,-0.65c-0.61,-0.84 -1.042,-1.71 -1.282,-2.58a5.417,5.417 0 0 1 -0.154,-0.75l0,0.029z' id='svg_1' />
    </g>
  </g>
</svg>

const LikeBar = ({like, comment, nLikes, nComments, isLiked}) => (
  <div className={likeBarContainer}>
    <span className={btn} onClick={like}> {getHeart(isLiked)}</span>{' '}
    <span style={isLiked ? { color: red } : null} className={text}>{nLikes > 0 ? nLikes : null}</span>
    <span className={btn} style={{marginLeft: '10px'}} onClick={comment}>{bubble} </span>
    <span className={text}>{nComments !== 0 ? nComments : null}</span>
  </div>
)
LikeBar.propTypes = {
  like: PropTypes.func.isRequired,
  comment: PropTypes.func.isRequired,
  nLikes: PropTypes.number.isRequired,
  nComments: PropTypes.number.isRequired,
  isLiked: PropTypes.bool.isRequired
}
export default LikeBar
