import React from 'react'
import { Route, Switch } from 'react-router'
import { HomeContainer, NewPostContainer, LoginContainer, NotFoundContainer, PostContainer, UserProfileContainer } from 'containers'
import requireAuthentication from 'helpers/requireAuthentication'

const getRoutes = () => (
  <Switch>
    <Route exact path='/' component={HomeContainer} />
    <Route exact path='/posts' component={HomeContainer} />
    <Route path='/posts/category/:category/authored' component={HomeContainer} />
    <Route path='/posts/category/:category' component={HomeContainer} />
    <Route exact path='/questions' component={HomeContainer} />
    <Route path='/questions/category/:category/authored' component={HomeContainer} />
    <Route path='/questions/category/:category' component={HomeContainer} />
    <Route path='/comments/category/:category/authored' component={HomeContainer} />
    <Route path='/new' component={NewPostContainer} />
    <Route path='/posts/edit/:id' component={NewPostContainer} />
    <Route path='/posts/:id' component={PostContainer} />
    <Route path='/questions/:id' component={PostContainer} />
    <Route path='/login' component={LoginContainer} />
    <Route
      path='/user'
      component={requireAuthentication(UserProfileContainer)}
    />
    <Route path='*' component={NotFoundContainer} />
  </Switch>
)

export default getRoutes
