from django.shortcuts import get_object_or_404
from django_rest_logger import log
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import GenericAPIView, UpdateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.models import User
from accounts.serializers import UserRegistrationSerializer, UserSerializer, UserWithIdSerializer, UserUpdateSerialzier, UserActivationSerializer, UserActiveStatusSerializer
from lib.utils import AtomicMixin

from comments.models import Comment
from comments.serializers import CommentCreationSerializer, CommentSerializer

from posts.models import Post
from posts.serializers import PostCreationSerializer, PostSerializer

from rest_framework import authentication
from rest_framework import exceptions

# class BasicCustomAuthentication(authentication.BaseAuthentication):
    
#     def authenticate(self, request):
#         return super()
#         # email = request.META.get('X_EMAIL')
        # if not email:
        #     return None

        # try:
        #     user = User.objects.get(email=email)
        # except User.DoesNotExist:
        #     raise exceptions.AuthenticationFailed('No such user')

        # return (user, None)


class UserRegisterView(AtomicMixin, CreateModelMixin, GenericAPIView):
    serializer_class = UserRegistrationSerializer
    authentication_classes = ()

    def post(self, request):
        """
        User registration view.
        NOTE: Marshall to add post_likes and comment_likes 


        """
        response = self.create(request)
        response.data['post_likes'] = []
        response.data['comment_likes'] = []
        user = User.objects.latest('date_joined')

        token = AuthToken.objects.create(user)
        return Response({
            'user': response.data,
            'token': token
        })
#Verfies that a user is logged in by checking the jwt token
class UserLoginTokenView(GenericAPIView):
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        """Process GET request and return user info."""
        """ TODO: replace with token decoded user request.token (get user info from db)"""
        """ refer to https://github.com/James1345/django-rest-knox/blob/master/knox/auth.py """
        token = AuthToken.objects.create(request.user)
        return Response({
            'user': self.get_serializer(request.user).data,
            'token': token
        }, status=status.HTTP_200_OK)

#Double check user credentials and logs them into the system
class UserLoginView(GenericAPIView):
    serializer_class = UserSerializer
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    

    def post(self, request):
        """User login with username and password."""
        token = AuthToken.objects.create(request.user)
        return Response({
            'user': self.get_serializer(request.user).data,
            'token': token
        })


class UserConfirmEmailView(AtomicMixin, GenericAPIView):
    serializer_class = None
    authentication_classes = ()

    def get(self, request, activation_key):
        """
        View for confirm email.

        Receive an activation key as parameter and confirm email.
        """
        user = get_object_or_404(User, activation_key=str(activation_key))
        if user.confirm_email():
            return Response(status=status.HTTP_200_OK)

        log.warning(message='Email confirmation key not found.',
                    details={'http_status_code': status.HTTP_404_NOT_FOUND})
        return Response(status=status.HTTP_404_NOT_FOUND)


class UserEmailConfirmationStatusView(GenericAPIView):
    serializer_class = None
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """Retrieve user current confirmed_email status."""
        user = self.request.user
        return Response({'status': user.confirmed_email}, status=status.HTTP_200_OK)


#Using a User token and a string with the category to get all these logged in Users Posts
class UserCommentsView(GenericAPIView):
    serializer_class = CommentSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, category):
        if((category == 'all') | (category == 'All' )):
              comments = Comment.objects.filter(author = request.user.id).order_by("-date_created")
        else:   
            comments = Comment.objects.filter(category=category,author = request.user.id).order_by('-date_created')
        posts = Post.objects.filter(id__in = comments.values('post_id'))
        PostUsers = User.objects.filter(id__in = posts.values('author'))
        user = User.objects.filter(id=request.user.id)
        AllUsers = user | PostUsers

        uSerializer = UserWithIdSerializer(AllUsers, many = True)
        serailzer = CommentSerializer(comments, many = True)
        pSerializer = PostSerializer(posts, many = True)
        
        return Response({"users":uSerializer.data,"comments":serailzer.data,"posts":pSerializer.data},status=status.HTTP_200_OK)


    def post(self, request, category):
        if((category == 'all') | (category == 'All' )):
              comments = Comment.objects.filter(author = request.user.id).order_by("-date_created")
        else:   
            comments = Comment.objects.filter(category=category,author = request.user.id).order_by('-date_created')
        posts = Post.objects.filter(id__in = comments.values('post_id'))
        PostUsers = User.objects.filter(id__in = posts.values('author'))
        user = User.objects.filter(id=request.user.id)
        AllUsers = user | PostUsers

        uSerializer = UserWithIdSerializer(AllUsers, many = True)
        serailzer = CommentSerializer(comments, many = True)
        pSerializer = PostSerializer(posts, many = True)
        
        return Response({"users":uSerializer.data,"comments":serailzer.data,"posts":pSerializer.data},status=status.HTTP_200_OK)




#Using a user toek an da string for the categories gets all the posts that the specific user authored
class UserPostsView(GenericAPIView):
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, category):   
        uSerializer = UserWithIdSerializer(request.user)
        UresponseList = list()
        UresponseList.append(uSerializer.data)
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=False,author = request.user.id).order_by("-date_created")
        else:   
            posts = Post.objects.filter(is_question=False,category=category,author = request.user.id).order_by('-date_created')
        serailzer = PostSerializer(posts, many = True)
        return Response({"users":UresponseList,"posts":serailzer.data})



    def post(self, request, category):
        uSerializer = UserWithIdSerializer(request.user)
        UresponseList = list()
        UresponseList.append(uSerializer.data)
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=False,author = request.user.id).order_by("-date_created")
        else:   
            posts = Post.objects.filter(is_question=False,category=category,author = request.user.id).order_by('-date_created')
        serailzer = PostSerializer(posts, many = True)
        return Response({"users":UresponseList,"posts":serailzer.data})


#Using a user toek an da string for the categories gets all the posts that the specific user authored
class UserQuestionsView(GenericAPIView):
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, category):      
        uSerializer = UserWithIdSerializer(request.user)
        UresponseList = list()
        UresponseList.append(uSerializer.data)
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=True,author = request.user.id).order_by("-date_created")
        else:   
            posts = Post.objects.filter(is_question=True, category=category, author = request.user.id).order_by('-date_created')
        serailzer = PostSerializer(posts, many = True)
        return Response({"users":UresponseList,"posts":serailzer.data})

    def post(self, request, category):
        uSerializer = UserWithIdSerializer(request.user)
        UresponseList = list()
        UresponseList.append(uSerializer.data)
        if((category == 'all') | (category == 'All' )):
            posts = Post.objects.filter(is_question=True,author = request.user.id).order_by("-date_created")
        else:   
            posts = Post.objects.filter(is_question=True, category=category, author = request.user.id).order_by('-date_created')
        serailzer = PostSerializer(posts, many = True)
        return Response({"users":UresponseList,"posts":serailzer.data})

    




#Allows a logged in user to update user profile settings
class UserUpdateView(UpdateAPIView):
        queryset = User.objects.all()
        serializer_class = UserUpdateSerialzier
        authentication_classes = (TokenAuthentication,)
        permission_classes = (IsAuthenticated,)
        
        
        def put(self, request):
            user = get_object_or_404(User, id = request.user.id)
            if('email' in request.data):
                if(request.data['email'] != ""):
                    user.email = request.data['email']
            if('first_name' in request.data):
                if(request.data['first_name'] != ""):
                    user.first_name = request.data['first_name']
            if('last_name' in request.data):                
                if(request.data['last_name'] != ""):
                    user.last_name = request.data['last_name']
            if('password' in request.data):
                if(request.data['password'] != ""):
                    user.set_password(request.data['password'])
            user.save()
            uList = list()
            serailzer = UserWithIdSerializer(user)
            uList.append(serailzer.data)
            return Response({"users":uList})

#makes a user deactivate not allowing them login or prefroming any authorized actions
class DeactivateUserView(GenericAPIView):
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    def post(self, request):
        user = get_object_or_404(User, id  = request.user.id)
        if(user.is_active):
            user.is_active = False
        user.save()
        uList = list()
        serailzer = UserActiveStatusSerializer(user)
        uList.append(serailzer.data)
        return Response({"users":uList})


#Allows a user to reactivate thier account by entering the email and password of the account(does not require authorization)
class ActivateUserView(GenericAPIView):
    serializer_class = UserActivationSerializer
    authentication_classes = (TokenAuthentication,)
    queryset = User.objects.all()
    def post(self,request):
        uList = list()
        user = get_object_or_404(User, email = request.data['email'])
        if user.check_password(request.data['password']):
                user.is_active = True
                user.save()
        else:
            return Response(status = status.HTTP_404_NOT_FOUND)
        serailzer = UserActiveStatusSerializer(user)
        uList.append(serailzer.data)
        return Response({"users":uList})
        
        