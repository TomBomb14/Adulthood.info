from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import accounts.views

urlpatterns = [
    url(_(r'^register/$'),
        accounts.views.UserRegisterView.as_view(),
        name='register'),

    #Use for initial login
    url(_(r'^login/$'),
        accounts.views.UserLoginView.as_view(),
        name='login'),
    #Use for every route auth
    url(_(r'^loginWithToken/$'),
        accounts.views.UserLoginTokenView.as_view(),
        name='loginToken'),
    url(_(r'^confirm/email/(?P<activation_key>.*)/$'),
        accounts.views.UserConfirmEmailView.as_view(),
        name='confirm_email'),
    url(_(r'^status/email/$'),
        accounts.views.UserEmailConfirmationStatusView.as_view(),
        name='status'),
    url(_(r'^UserPosts/(?P<category>\w{1,50})$'),
        accounts.views.UserPostsView.as_view(),
        name='UserPosts'),
    url(_(r'^UserQuestions/(?P<category>\w{1,50})$'),
        accounts.views.UserQuestionsView.as_view(),
        name='UserQuestions'),
    url(_(r'^UserComments/(?P<category>\w{1,50})$'),
        accounts.views.UserCommentsView.as_view(),
        name='UserComments'),
    url(_(r'^UpdateUser/'),
        accounts.views.UserUpdateView.as_view(),
        name='UpdateUser'),
    url(_(r'^DeActivateUser/$'),
        accounts.views.DeactivateUserView.as_view(),
        name='DeActivateUser'),
    url(_(r'^ActivateUser$'),
        accounts.views.ActivateUserView.as_view(),
        name='DeleteUser'),

]
