from rest_framework import serializers

from accounts.models import User
from likes.models import PostLikes, CommentLikes
from lib.utils import validate_email as email_is_valid
from collections import OrderedDict



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',)


    #customer representation that uses an orderdict to create a consistent structure for the json response
    def to_representation(self, instance):
        """
            NOTE: Gets the user id likes
        """
        rep = {}
        # initialize on ordereddict
        rep=OrderedDict()
        
        rep['email'] = str(instance.email)
        rep['first_name'] = instance.first_name
        rep['last_name'] = instance.last_name


        postLikes = PostLikes.objects.filter(user_id_id = instance.id)
        commentLikes = CommentLikes.objects.filter(user_id_id = instance.id)

        userPostLikes = list()
        userCommentLikes = list()


        #Append any post like ids to userPostLikes
        for c in postLikes:
            userPostLikes.append(str(c.post_id_id))
        rep['post_likes'] = userPostLikes

        #Append any comment like ids to userPostLikes
        for c in commentLikes:
            userCommentLikes.append(str(c.reply_id_id))
        rep['comment_likes'] = userCommentLikes
        return rep



class UserWithIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'is_active')


    #customer representation that uses an orderdict to create a consistent structure for the json response
    def to_representation(self, instance):
        """
            NOTE: Gets the user id likes
        """
        rep = {}
        # initialize on ordereddict
        rep=OrderedDict()
        
        rep['email'] = str(instance.email)
        rep['first_name'] = instance.first_name
        rep['last_name'] = instance.last_name

        postLikes = PostLikes.objects.filter(user_id_id = instance.id)
        commentLikes = CommentLikes.objects.filter(user_id_id = instance.id)

        userPostLikes = list()
        userCommentLikes = list()


        #Append any post like ids to userPostLikes
        for c in postLikes:
            userPostLikes.append(str(c.post_id_id))
        rep['post_likes'] = userPostLikes

        #Append any comment like ids to userPostLikes
        for c in commentLikes:
            userCommentLikes.append(str(c.reply_id_id))
        rep['comment_likes'] = userCommentLikes
        return rep

        
class UserUpdateSerialzier(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password')

class UserActivationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password')

class UserActiveStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'is_active')

class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'password')

    def create(self, validated_data):
        """
        Create the object.

        :param validated_data: string
        """
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user

    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """

        if not email_is_valid(value):
            raise serializers.ValidationError('Please use a different email address provider.')

        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError('Email already in use, please use a different email address.')

        return value
