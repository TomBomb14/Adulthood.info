from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
import category.views

urlpatterns = [
            #A hardcoded list of categories
    url(_(r'^list$'),
        category.views.CategoryList.as_view(),
        name='GetCategoryList'),
]
