from rest_framework.views import APIView
from rest_framework.response import Response


class CategoryList(APIView):
    """
        A hardcoded list of categories
    """

    def get(self, request, format=None):
        return Response(['home','finance', 'health', 'career', 'laws'])


