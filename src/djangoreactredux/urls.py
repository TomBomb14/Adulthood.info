from django.conf import settings
from django.conf.urls import include, url
from django.views.decorators.cache import cache_page
from django.contrib import admin


from base import views as base_views

urlpatterns = [
    url(r'^api/v1/accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^api/v1/getdata/', include('base.urls', namespace='base')),
    url(r'^api/v1/posts/', include('posts.urls', namespace='posts')),
    url(r'^api/v1/comments/', include('comments.urls', namespace='comments')),
    url(r'^api/v1/category/', include('category.urls', namespace='category')),
    url(r'^api/v1/likes/', include('likes.urls', namespace='likes')),

    url(r'^admin/', include(admin.site.urls)),

    # catch all others because of how history is handled by react router - cache this page because it will never change
    url(r'', cache_page(settings.PAGE_CACHE_SECONDS)(base_views.IndexView.as_view()), name='index'),
]
