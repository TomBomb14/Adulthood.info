from django.shortcuts import get_object_or_404
from django_rest_logger import log
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import GenericAPIView, UpdateAPIView , DestroyAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


from lib.utils import AtomicMixin
from django.http import QueryDict

from posts.models import Post
from posts.serializers import PostCreationSerializer, PostSerializer

from accounts.models import User
from accounts.serializers import UserSerializer, UserWithIdSerializer

from likes.models import PostLikes, CommentLikes
from likes.serializers import (PostLikeCreationSerializer, PostLikeSerializer, PostLikeDeleteSerializer, 
                                CommentLikeCreationSerializer, CommentLikeDeleteSerializer)

from rest_framework import authentication
from rest_framework import exceptions

class LikePostCreationView(GenericAPIView):
    serializer_class = PostLikeCreationSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


    def post(self, request):
        """
    	Like Creation view.
    	
    	// - Get the user_id and post_id from the front-end
    	   - After, insert into db
               - Return the new like
    	"""
        request.data['user_id'] = self.request.user.id
        serializer = PostLikeCreationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            like = PostLikes.objects.all().filter(user_id_id=self.request.user.id)
            userPostLikes = list()
            #Append any post like ids to userPostLikes
            for c in like:
                userPostLikes.append(str(c.post_id_id))


            return Response({
                'success': True,
                'posts_likes_ids': userPostLikes
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#view that is used to delete a new post
class LikePostDeleteView(DestroyAPIView):
    queryset = PostLikes.objects.all()
    serializer_class = PostLikeDeleteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def destroy(self, request, *args, **kwargs):
        """
            TODO: Need to figure out a way to return 404 pk is unknown

        """
        like = PostLikes.objects.get(user_id_id=self.request.user.id, post_id_id=self.request.data['post_id'])
        like.delete()
        like = PostLikes.objects.all().filter(user_id_id=self.request.user.id)
        userPostLikes = list()
        #Append any post like ids to userPostLikes
        for c in like:
            userPostLikes.append(str(c.post_id_id))

        return Response({
            'success': True,
            'posts_likes_ids': userPostLikes
        }, status=status.HTTP_200_OK)





class LikeCommentCreationView(GenericAPIView):
    serializer_class = CommentLikeCreationSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


    def post(self, request):        
     
        request.data['user_id'] = self.request.user.id
        serializer = CommentLikeCreationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()           
            like = CommentLikes.objects.all().filter(user_id_id=self.request.user.id)
            userCommentLikes = list()
            #Append any post like ids to userPostLikes
            for c in like:
                userCommentLikes.append(str(c.reply_id_id))

            return Response({
                'success': True,
                'comments_likes_ids': userCommentLikes
                }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#view that is used to delete a new post
class LikeCommentDeleteView(DestroyAPIView):
    queryset = CommentLikes.objects.all()
    serializer_class = CommentLikeDeleteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def destroy(self, request, *args, **kwargs):
        """
            TODO: Need to figure out a way to return 404 pk is unknown

        """
        like = CommentLikes.objects.get(user_id_id=self.request.user.id, reply_id_id=self.request.data['reply_id'])
        like.delete()

        cLike = CommentLikes.objects.all().filter(user_id_id=self.request.user.id)
        userCommentLikes = list()
            #Append any post like ids to userPostLikes
        for c in cLike:
            userCommentLikes.append(str(c.reply_id_id))

            return Response({
                'success': True,
                'comments_likes_ids': userCommentLikes
            }, status=status.HTTP_200_OK)





        
        

