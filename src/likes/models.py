
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from accounts.models import User
from posts.models import Post
from comments.models import Comment

class PostLikes(models.Model):
    user_id = models.ForeignKey(User, related_name = "post_user_id_likes", on_delete=models.CASCADE)
    post_id = models.ForeignKey(Post, related_name = "post_id_likes", on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now = True)
    
    def create_likes(self, user_id, post_id, date_created):
        """
        Create and save likes

        :param attached_post: Post
        :param author: User
        :param datecreated: date
        :param extra_fields:
        :return: comment
        """
        now = timezone.now()
        like = self.model(
                    user_id=user_id,
                    post_id=post_id,
                    date_created=now,
                )
        like.save(using=self._db)
        return like


class CommentLikes(models.Model):
    user_id = models.ForeignKey(User, related_name = "comment_user_id_likes", on_delete=models.CASCADE)
    reply_id = models.ForeignKey(Comment, related_name = "reply_id_likes", on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now = True)
    
    def create_likes(self, user_id, reply_id, date_created):
        """
        Create and save likes

        :param attached_post: Post
        :param author: User
        :param datecreated: date
        :param extra_fields:
        :return: comment
        """
        now = timezone.now()
        like = self.model(
                    user_id=user_id,
                    reply_id=reply_id,
                    date_created=now,
                )
        like.save(using=self._db)
        return like




    
      

