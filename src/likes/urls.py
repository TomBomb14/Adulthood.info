from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import likes.views


urlpatterns = [
    url(_(r'^post/create/$'),
        likes.views.LikePostCreationView.as_view(),
        name='CreatePostLike'),
    url(_(r'^post/delete/$'),
        likes.views.LikePostDeleteView.as_view(),
        name='DeletePostLike'),
    url(_(r'^comment/create/$'),
        likes.views.LikeCommentCreationView.as_view(),
        name='CreateCommentLike'),
    url(_(r'^comment/delete/$'),
        likes.views.LikeCommentDeleteView.as_view(),
        name='DeleteCommentLike'),
]
