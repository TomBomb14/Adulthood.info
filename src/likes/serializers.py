from rest_framework import serializers
#imports users and user serailzers 
from accounts.models import User
from accounts.serializers import UserWithIdSerializer
from posts.models import Post
from likes.models import PostLikes, CommentLikes
from django.db import models
from collections import OrderedDict
import datetime


#Seriaizer that accepts incoming user data and creates the Like
class PostLikeCreationSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = PostLikes
        fields = ('id', 'user_id', 'post_id')
        
    #creates a like with validated_data
    def create(self, validated_data):
        #import pdb
        #pdb.set_trace()  
            
       	like = PostLikes.objects.create(user_id = validated_data['user_id'],
                                    post_id = validated_data['post_id'])
        return like
   
#Serializer that deletes a Comment
class PostLikeDeleteSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = PostLikes
        fields = '__all__'

           
#serailzer for  Comments that serailzer and deserailzer data
class PostLikeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = PostLikes
        fields = ('id', 'user_id','post_id','date_created')

    def to_representation(self, instance):
        rep = {}
            
        # initialize on ordereddict
        rep=OrderedDict()
        rep['id'] = str(instance.id)
        rep['user_id'] = str(instance.user_id.id)
        rep['post_id'] = str(instance.post_id.id)
        rep['date_created'] = int(instance.date_created.timestamp())
        return rep




#********************************COMMENTS*******************************#

#Seriaizer that accepts incoming user data and creates the Like
class CommentLikeCreationSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = CommentLikes
        fields = ('id', 'user_id', 'reply_id')
        
    #creates a like with validated_data
    def create(self, validated_data):
        #import pdb
        #pdb.set_trace()  
            
        like = CommentLikes.objects.create(user_id = validated_data['user_id'],
                                    reply_id = validated_data['reply_id'])
        return like
   
#Serializer that deletes a Comment
class CommentLikeDeleteSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = CommentLikes
        fields = '__all__'

           
#serailzer for  Comments that serailzer and deserailzer data
class CommentLikeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = CommentLikes
        fields = ('id', 'user_id','reply_id','date_created')

    def to_representation(self, instance):
        rep = {}
            
        # initialize on ordereddict
        rep=OrderedDict()
        rep['id'] = str(instance.id)
        rep['user_id'] = str(instance.user_id.id)
        rep['reply_id'] = str(instance.reply_id.id)
        rep['date_created'] = int(instance.date_created.timestamp())
        return rep


 
            





        






        

